#ifndef PEOPLEDETECTION_H
#define PEOPLEDETECTION_H

#include <QObject>
#include <QSerialPort>
#include <QTimer>
#include <QVector>
#include <QFile>

#include <stdio.h>
#include <stdarg.h>

#include "SensorData.pb.h"

#include "config.h"

#define SERIAL_DATA_BUFFER_MAX_LANGTH_BYTES                                     (300)
#define DEBUG_OUTPUT_LINE_BUFFER_SIZE_BYTES                                     (130)

#define TWO_ADC_NODE_ID_LOWER_BOUND                                             (4)
#define TWO_ADC_NODE_ID_UPPER_BOUND                                             (7)

#define FOUR_ADC_NODE_ID_LOWER_BOUND                                            (1)
#define FOUR_ADC_NODE_ID_UPPER_BOUND                                            (3)

#define CSV_SPLIT_SYMBOL                                                        ','

#define CSV_FILE_TIME_DATA_COLUMN_INDEX                                         (0)
#define CSV_FILE_WORKING_MODE_DATA_COLUMN_INDEX                                 (1)

#define CSV_FILE_PWM_DATA_COLUMN_INDEX                                          (2)
#define CSV_FILE_TEMPERATURE_DATA_COLUMN_INDEX                                  (3)
#if ENABLE_HUMIDITY_PARSING
#define CSV_FILE_HUMIDITY_DATA_COLUMN_INDEX                                     (4)
#define CSV_FILE_ADC0_DATA_COLUMN_INDEX                                         (5)
#define CSV_FILE_ADC1_DATA_COLUMN_INDEX                                         (6)
#define CSV_FILE_MOTION_DATA_COLUMN_INDEX                                       (7)

#define CSV_FILE_WORKING_MODE_SPLIT_LIST_SIZE                                   (8)
#else // !ENABLE_HUMIDITY_PARSING
#define CSV_FILE_ADC0_DATA_COLUMN_INDEX                                         (4)
#define CSV_FILE_ADC1_DATA_COLUMN_INDEX                                         (5)
#define CSV_FILE_MOTION_DATA_COLUMN_INDEX                                       (6)

#define CSV_FILE_WORKING_MODE_SPLIT_LIST_SIZE                                   (7)
#endif // ENABLE_HUMIDITY_PARSING
#define CSV_FILE_SLEEPING_MODE_SPLIT_LIST_SIZE                                  (2)

// Parsing bytes
// Client message
#define CLIENT_IDENTIFY_BYTE                                                    '%'

// Server message
#define SERVER_IDENTIFY_BYTE                                                    '$'
#define SERVER_DATA_BUFFER_SPLIT_BYTE                           				'|'

#define SERVER_NODE_COLLECTED_DATA_INDICATION_BYTE                              'C'
#define SERVER_NODE_POWER_STATUS_INDICATION_BYTE                                'P'
#define SERVER_NODE_TESTING_INDICATION_BYTE                                     'T'
#define SERVER_NODE_REGISTER_INDICATION_BYTE                                    'R'

#define GENERAL_BUFFER_END_BYTE                                                 '#'

#define START_FRAME_DATA_SYMBOL                                                 'N'
#define WORKING_DATA_SYMBOL                                                     'W'
#define SLEEPING_INTERRUPT_DATA_SYMBOL                                          'S'

// Positions
#define GENERAL_IDENTIFY_BYTE_POSITION                                          (0)
// Client message
#define CLIENT_PROTOBUF_DATA_LENGTH_POSITION                                	(1)
#define CLIENT_PROTOBUF_DATA_START_POSITION                                     (2)
// Server message
#define SERVER_NODE_ADDRESS_PARSING_START_POSITION                              (1)
#define SERVER_NODE_ADDRESS_START_POSITION                                      (3)
#define SERVER_NODE_ADDRESS_END_POSITION                                        (7)
#define SERVER_NODE_RSSI_START_POSITION                                         (8)
#define SERVER_NODE_RSSI_END_POSITION                                           (11)
#define SERVER_NODE_PROTOBUF_DATA_LENGTH_POSITION               				(12)
#define SERVER_NODE_PROTOBUF_DATA_INDICATION_BYTE_START_POSITION                (13)
#define SERVER_NODE_PROTOBUF_DATA_START_POSITION                                (14)

// Lengths
#define SERVER_NODE_PREFIX_BUFFER_FIXED_LENGTH                                  (11)

// Parsing reading lengths
#define PARSING_STATE_IDENTIFY_BYTE_REQUIRED_LENGTH                             (1)
#define PARSING_STATE_LAST_ENDING_BYTE_REQUIRED_LENGTH                          (1)
// client message
#define PARSING_STATE_CLIENT_MSG_PROTOBUF_BUFFER_LEN_REQUIRED_LENGTH            (1)
// servers' message
#define PARSING_STATE_SERVER_ADDRESS_RSSI_BUFFER_REQUIRED_LENGTH                (11)
#define PARSING_STATE_SERVER_MSG_PROTOBUF_BUFFER_LEN_REQUIRED_LENGTH            (1)

#if ENABLE_DEBUGGING_PRINTS
#define NECESSARY_DEBUG(str)                                                    \
    qDebug() << str

#define NECESSARY_LOG(...)                                                      \
    do                                                                          \
    {                                                                           \
        printf(__VA_ARGS__);                                                    \
        cout << flush;                                                          \
    } while (0)

#define CONDITIONAL_LOG(expr, format, ...)                                      \
    do                                                                          \
    {                                                                           \
        if (expr)                                                               \
        {                                                                       \
            snprintf(debugOutputLineBuffer, DEBUG_OUTPUT_LINE_BUFFER_SIZE_BYTES, format, ##__VA_ARGS__);           \
            cout << debugOutputLineBuffer;                                      \
            cout << flush;                                                      \
        }                                                                       \
    } while (0)
#else // !ENABLE_DEBUGGING_PRINTS
#define NECESSARY_DEBUG(str)
#define NECESSARY_LOG(...)
#define CONDITIONAL_LOG(expr, format, ...)
#endif // ENABLE_DEBUGGING_PRINTS

#define SERIAL_CONNECTION_MONITOR_TIMEOUT_MS                                    (1000*90)
#define OUTPUT_UPDATING_TIMEOUT_MS                                              (1000*60)
#define NODE_CONNECTION_MONITOR_TIMEOUT_MS                                      (1000*120)

#define FILE_LOGGING_BUFFER_SIZE_BYTES                                          (50)
#define DEFAULT_LOGGING_FILE_TYPE_STRING                                        ".txt"

// Algorithm1
#define DEFAULT_THRESHOLD_OCCUPANCY_SWITCH_SCALE                                (0.1f)
#define DEFAULT_THRESHOLD_NON_MOTION_LENGTH                                     (60)
#define PEAK_PEAK_VALUE_TOO_SMALL_THRESHOLD                                     (0.1f)
#define TWO_ADC_NODE_FORCED_INIT_ELEMENTS_NUMBER                                (3)
#define TWO_ADC_NODE_FORCED_INIT_MAX_TIMES                                      (12)
#define TWO_ADC_FRAME_LINE_CONTENT_MIN_NUMBER                                   (160)
#define INIT_FILTER_VPP_THRESHOLD_VALUE                                         (0.05f)

// Algorithm2
#define PEAK_THREHOLD                                                           (0.25f)
#define ADC_ZERO                                                                (1.1f)
#define MINUM_PEAK_DIS                                                          (5)
#define MAX_ADC_NUMBER                                                          (4)

#define PEOPLE_ENTER_DIRECTION_VALUE                                            (1)
#define PEOPLE_EXIT_DIRECTION_VALUE                                             (-1)
#define PEOPLE_NEITHER_ENTER_EXIT_DIRECTION_VALUE                               (0)

#define CHILLING_TIMER_TIMEOUT_MS                                               (1*1000)
#define FORCED_CALCULATION_TIMER_TIMEOUT_MS                                     (10*1000)

#define DEFAULT_OCCUPANCY_NUM                                                   (0)
#define DEFAULT_OCCUPANCY_EXIST_NUM                                             (1)

#define OVERALL_RESET_TIMER_TIMEOUT_HOURS                                       (4)
#define OVERALL_RESET_TIMER_TIMEOUT_MS                                          (OVERALL_RESET_TIMER_TIMEOUT_HOURS * 3600 * 1000)
//#define OVERALL_RESET_TIMER_TIMEOUT_MS                                          (5 * 60 * 1000)//OVERALL_RESET_TIMER_TIMEOUT_HOURS * 3600 * 1000)

#define DETECTION_RESULT_NODES_ISSUE                                            "Some nodes or hub have issues to solve first"

#define FOUR_ADC_FILE_NAME_MIDDLE_PART                                          "4ADC"
#define TWO_ADC_FILE_NAME_MIDDLE_PART                                           "2ADC"

#define NODE_RAW_DATA_LOGGING_FILENAME_STRING                                   "_raw_data"
#define NODE_RESULTS_LOGGING_FILENAME_STRING                                    "_computation_result"

class MainWindow;

typedef enum
{
    NODE_TYPE_TWO_ADC,
    NODE_TYPE_FOUR_ADC,
    NODE_TYPE_NULL
}nodeType_t;

typedef enum
{
    DETECTION_STATE_INIT,
    DETECTION_STATE_S1,
    DETECTION_STATE_S2,
    DETECTION_STATE_S3
}detectionState_t;

typedef enum
{
    error_return_index_for_identify_byte_length,
    error_return_index_for_identify_byte_parsing,
    error_return_index_for_client_protobuf_len,
    error_return_index_for_client_protobuf_data_len,
    error_return_index_for_client_protobuf_data_parsing,
    error_return_index_for_server_address_rssi_buffer_len,
    error_return_index_for_server_address_rssi_buffer_parsing,
    error_return_index_for_server_protobuf_len,
    error_return_index_for_server_protobuf_indication_byte_len,
    error_return_index_for_server_protobuf_indication_byte_parsing,
    error_return_index_for_server_protobuf_data_len,
    error_return_index_for_server_protobuf_data_parsing,
    error_return_index_for_end_byte_parsing,
    error_return_index_for_none
} errorReturnIndex_t;

#if ENABLE_ALGORITHM1_CALCULATION
// algorithm1 required structures
typedef struct
{
    float adc0PeakPeakVoltage;
    float adc1PeakPeakVoltage;
    float currentTemperature;
    uint8_t interruptCounter;
    uint8_t gpioMotionCounter;
    uint8_t ifExistMotion;
}oneFrameAnalysisResult_t;

typedef struct
{
    bool ifInitialed;
    uint8_t initTimesCounter;
    uint8_t id;
    bool ifRegistered;
    QStringList linesContent;

    QVector<float> vecPeriodTemperature;

    QVector<oneFrameAnalysisResult_t> vecInitFrameAnalysis;
    QVector<oneFrameAnalysisResult_t> vecFrameAnalysisResults;
    QVector<oneFrameAnalysisResult_t> vecFrameAnalysisResultsA;
    QVector<oneFrameAnalysisResult_t> vecFrameAnalysisResultsB;
    QVector<int> occupancyStateSaved;
    int occupancyState;
    int occupancySwitch;
    int motionLength;
    int AState;
}twoADCNodeDetection_t;
#endif // ENABLE_ALGORITHM1_CALCULATION

#if ENABLE_ALGORITHM2_CALCULATION
// algorithm2 required structures
typedef struct
{
    int index;
    float adcValue;
    float adcAbsDifference;
}peakData_t;

typedef struct
{
    int adcIndex;
    float peakDataIndexMean;
}dSave_t;

typedef struct
{
    uint8_t id;
    bool ifRegistered;
    // analysis
    bool ifMotionInOnePeriod;
    bool ifCalculationInProgress;
    bool ifForcedToCalculate;
    bool ifInChillingProcess;

    QTimer *forcedCalculationTimer;
    QTimer *chillTimer;

    QStringList linesContent;

    bool ifGPIOHigh;
    QVector<peakData_t*> vecPeakDataPointers[MAX_ADC_NUMBER];                          // 元素最后释放
    QVector<peakData_t*> vecPeakDataPointersFilter[MAX_ADC_NUMBER];    // 元素不释放
    QVector<dSave_t*> vecDSavePointers;
    QVector<float> vecPIRDataArray[MAX_ADC_NUMBER];
    QVector<int> vecOrder;
}fourADCNodeDetection_t;
#endif // ENABLE_ALGORITHM2_CALCULATION

typedef struct
{
    nodeType_t type;
    uint16_t address;
#if DATA_SAVING_WHILE_COMPUTING
    QFile rawDataLoggingFile;
    QFile algorithmResultsLoggingFile;
#endif // DATA_SAVING_WHILE_COMPUTING

#if (ENABLE_ALGORITHM1_CALCULATION || ENABLE_ALGORITHM2_CALCULATION)
    union
    {
#if ENABLE_ALGORITHM1_CALCULATION
        twoADCNodeDetection_t* twoADCDetectionPtr;
#endif // ENABLE_ALGORITHM1_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
        fourADCNodeDetection_t* fourADCDetectionPtr;
#endif // ENABLE_ALGORITHM2_CALCULATION
    };
#endif // (ENABLE_ALGORITHM1_CALCULATION || ENABLE_ALGORITHM2_CALCULATION)
}nodeDetail_t;

class PeopleDetection : public QObject
{
    Q_OBJECT
public:
    explicit PeopleDetection(QObject *parent = nullptr);
    PeopleDetection(MainWindow *mainWindowPtr,
                    QSerialPort *mSerial);
    ~PeopleDetection();

    void handleSerialData(void);

signals:

public slots:

private slots:
    void serialMonitorTimerTimeout(void);
#if ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
    void updatingOccupancyResultTimeout(void);
#endif // ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
    void nodeDetectionInit(void);
#if ENABLE_ALGORITHM_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
    void algorithm2ChillingTimerTimeout(void);
    void algorithm2ForcedCalculationTimerTimeout(void);
#endif // ENABLE_ALGORITHM2_CALCULATION
#endif // ENABLE_ALGORITHM_CALCULATION
    void overallResetTimerTimeout(void);
    void dateAdvancedHandle(QString &currentDateString);

private:
    void parseSerialPacket(void);
    void handleClientNotification(void);
    void handleServerSensoredData(uint16_t nodeAddress);

#if ENABLE_ALGORITHM_CALCULATION
#if ENABLE_ALGORITHM1_CALCULATION
    int algorithm1GetMaxDataIndexFromRange(QVector<float> &data, int startIndex, int endIndex);
    int algorithm1GetMinDataIndexFromRange(QVector<float> &data, int startIndex, int endIndex);
    float algorithm1ComputePeakPeakVoltage(QVector<float> &data, int startIndex, int endIndex);
    void algorithm1GetPeakPeakDataAndMore(nodeDetail_t &nodeDetail);
    void algorithm1DoAlgorithmComputation(nodeDetail_t &nodeDetail);//twoADCNodeDetection_t *nodeDetection_ptr);
#endif // ENABLE_ALGORITHM1_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
    void algorithm2DetectPeak(nodeDetail_t &nodeDetail, int ADCIndex);
    void algorithm2PeakFilter(nodeDetail_t &nodeDetail, int ADCIndex);
    void algorithm2DetectDirection(nodeDetail_t &nodeDetail);
    int algorithm2GetDirection(nodeDetail_t &nodeDetail);
    void algorithm2GetDirectionResult(nodeDetail_t &nodeDetail);
#endif // ENABLE_ALGORITHM2_CALCULATION
#endif // ENABLE_ALGORITHM_CALCULATION

    MainWindow *mainWindowPtr = nullptr;

    // Serial parser
    QSerialPort *mSerial = nullptr;
    char serialReadBuffer[SERIAL_DATA_BUFFER_MAX_LANGTH_BYTES];
    bool ifIdentifyByteMet;
    size_t numOfSerialReadBufferBytesSaved;
    char copiedTailChar;
    QTimer *serialConnectionMonitorTimer;
    // Serial parsing protobuf
    NES_SensorData sensorData;
    NES_ClientNotification clientNotification;
    NES_SensorRegisterStatus sensorRegisterStatus;

    // Debug
    char debugOutputLineBuffer[DEBUG_OUTPUT_LINE_BUFFER_SIZE_BYTES];
    bool ifConditionalLoggingOn = false;
    bool ifConditionalMiscLoggingOn = true;
    bool ifDynamicSamplingLoggingOn = false;
    bool ifErrorLoggingOn = false;
    bool ifDynamicResultsLoggingOn = true;

    // Nodes
    nodeDetail_t nodeDetails[MAX_NODE_NUMBER];
    QVector<uint32_t> vecClientGPIOStatus;

#if ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
    QTimer *outputUpdatingTimer;
#endif // ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION

    // detection related
    uint8_t registeredNodesNum;
    uint8_t initOKNodesNum;
    int floatPrecision;
    detectionState_t detectionState;
    int occupancyNum;
    QTimer* overallResetTimer;
    uint16_t conflictNodeAddress;
};

#endif // PEOPLEDETECTION_H
