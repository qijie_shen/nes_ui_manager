#include "peopledetection.h"
#include "mainwindow.h"

#include <QtDebug>
#include <QTime>

#include <string.h>
#include <stdlib.h>

#include <string>

using namespace std;

#if defined(Q_OS_LINUX)
#include <wiringPi.h>
#include <wiringPiI2C.h>

#define QT_ON_RPI
#endif // Q_OS_LINUX

#if ENABLE_DEBUGGING_PRINTS
static const char *serialDataParsingErrorReturnMsgs[] =
{
    "error_return_index_for_identify_byte_length",
    "error_return_index_for_identify_byte_parsing",
    "error_return_index_for_client_protobuf_len",
    "error_return_index_for_client_protobuf_data_len",
    "error_return_index_for_client_protobuf_data_parsing",
    "error_return_index_for_server_address_rssi_buffer_len",
    "error_return_index_for_server_address_rssi_buffer_parsing",
    "error_return_index_for_server_protobuf_len",
    "error_return_index_for_server_protobuf_indication_byte_len",
    "error_return_index_for_server_protobuf_indication_byte_parsing",
    "error_return_index_for_server_protobuf_data_len",
    "error_return_index_for_server_protobuf_data_parsing",
    "error_return_index_for_end_byte_parsing",
    "error_return_index_for_none"
};
#endif // ENABLE_DEBUGGING_PRINTS

PeopleDetection::PeopleDetection(QObject *parent) : QObject(parent)
{

}

PeopleDetection::PeopleDetection(MainWindow *mainWindowPtr,
                                 QSerialPort *mSerial) :
    mainWindowPtr(mainWindowPtr),
    mSerial(mSerial)
{
    uint8_t i;

    ifIdentifyByteMet = false;
    numOfSerialReadBufferBytesSaved = 0;
    copiedTailChar = '\0';

    registeredNodesNum = 0;
    initOKNodesNum = 0;
    conflictNodeAddress = 0;

    //detectionState = DETECTION_STATE_INIT;
    //detectionState = DETECTION_STATE_S1;
    detectionState = DETECTION_STATE_S3;
    occupancyNum = DEFAULT_OCCUPANCY_EXIST_NUM;
#if !ENABLE_DEVELOPER_MODE
    floatPrecision = DEFAULT_FLOAT_PRECISION;
#endif // ENABLE_DEVELOPER_MODE

    for (i=0; i<MAX_NODE_NUMBER; i++)
    {
        nodeDetails[i].type = NODE_TYPE_NULL;
        nodeDetails[i].address = 0;
#if ENABLE_ALGORITHM1_CALCULATION
        nodeDetails[i].twoADCDetectionPtr = nullptr;
#endif // ENABLE_ALGORITHM1_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
        nodeDetails[i].fourADCDetectionPtr = nullptr;
#endif // ENABLE_ALGORITHM2_CALCULATION
    }

#if ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
    // Refresh timer setup
    outputUpdatingTimer = new QTimer(this);
    outputUpdatingTimer->start(OUTPUT_UPDATING_TIMEOUT_MS);
    connect(outputUpdatingTimer, SIGNAL(timeout()), this, SLOT(updatingOccupancyResultTimeout()));
#endif // ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION

    // Serial monitor timer setup
    serialConnectionMonitorTimer = new QTimer(this);
    connect(serialConnectionMonitorTimer, SIGNAL(timeout()), this, SLOT(serialMonitorTimerTimeout()));
    serialConnectionMonitorTimer->start(SERIAL_CONNECTION_MONITOR_TIMEOUT_MS);

    // overall reset timer setup
    overallResetTimer = new QTimer(this);
    connect(overallResetTimer, SIGNAL(timeout()), this, SLOT(overallResetTimerTimeout()));
    overallResetTimer->start(OVERALL_RESET_TIMER_TIMEOUT_MS);
    overallResetTimer->setSingleShot(false);

    connect(mainWindowPtr, SIGNAL(updateDatePartOfFileNames(QString&)),
            this, SLOT(dateAdvancedHandle(QString&)));

    // Pin output setup
    #if defined(Q_OS_LINUX)
    #ifdef QT_ON_RPI
    if(wiringPiSetup() < 0)
    {
        NECESSARY_LOG("wiringPiSetup failed\n");
    }
    else
    {
        pinMode(1, OUTPUT);
    }
    #endif // QT_ON_RPI
    #endif // Q_OS_LINUX
}

PeopleDetection::~PeopleDetection()
{

}

void PeopleDetection::handleSerialData(void)
{
    qint64 numberOfBytesAvailable;
    int numberOfBytesRead;
    int i, copyStartIndex;
    size_t dataCopySize;

    numberOfBytesAvailable = mSerial->bytesAvailable();
    if (numberOfBytesAvailable > 0)
    {
        const QByteArray data = mSerial->readAll();
        numberOfBytesRead = data.size();
        //NECESSARY_LOG("Really read from the port %d bytes\n", numberOfBytesRead);
        if (numberOfBytesRead > 0)
        {
            copyStartIndex = 0;
            for (i=0; i<numberOfBytesRead; i++)
            {
                if ((data.at(i) == CLIENT_IDENTIFY_BYTE) ||
                    (data.at(i) == SERVER_IDENTIFY_BYTE))
                {
                    if (i == 0)
                    {
                        if (numOfSerialReadBufferBytesSaved == 0)
                        {
                            ifIdentifyByteMet = true;
                        }
                    }
                    else
                    {
                        if (data.at(i-1) == GENERAL_BUFFER_END_BYTE)
                        {
                            dataCopySize = static_cast<size_t>(i - copyStartIndex);
                            memcpy((serialReadBuffer + numOfSerialReadBufferBytesSaved),
                                    data.data() + copyStartIndex,
                                    dataCopySize);
                            if (ifIdentifyByteMet)
                            {
                                numOfSerialReadBufferBytesSaved += dataCopySize;
                                // The existing buffer is good to parse.
                                //NECESSARY_LOG("got a total packet1, size is %llu\n", numOfSerialReadBufferBytesSaved);
                                // parsing
                                parseSerialPacket();
                            }
                            numOfSerialReadBufferBytesSaved = 0;
                            ifIdentifyByteMet = true;
                            copyStartIndex = i;
                        }
                    }
                }
            }

            dataCopySize = static_cast<size_t>(numberOfBytesRead - copyStartIndex);
            if (dataCopySize > 0)
            {
                if (ifIdentifyByteMet)
                {
                    memcpy(serialReadBuffer + numOfSerialReadBufferBytesSaved,
                            data.data() + copyStartIndex,
                            dataCopySize);
                    numOfSerialReadBufferBytesSaved += dataCopySize;
                    if (numOfSerialReadBufferBytesSaved <= SERIAL_DATA_BUFFER_MAX_LANGTH_BYTES)
                    {
                        copiedTailChar = serialReadBuffer[numOfSerialReadBufferBytesSaved-1];
                    }
                    else
                    {
                        NECESSARY_DEBUG("Memory error happens");
                        // Memory issue, just clear
                        numOfSerialReadBufferBytesSaved = 0;
                        copiedTailChar = '\0';
                        serialReadBuffer[GENERAL_IDENTIFY_BYTE_POSITION] = '\0';
                        ifIdentifyByteMet = false;
                    }
                }
            }

            if (numOfSerialReadBufferBytesSaved > 0)
            {
                // Check if current buffer forms a complete packet
                if (copiedTailChar == GENERAL_BUFFER_END_BYTE)
                {
                    if ((serialReadBuffer[GENERAL_IDENTIFY_BYTE_POSITION] == CLIENT_IDENTIFY_BYTE) ||
                        (serialReadBuffer[GENERAL_IDENTIFY_BYTE_POSITION] == SERVER_IDENTIFY_BYTE))
                    {
                        // A complete packet, just flush it
                        //NECESSARY_LOG("got a total packet2, size is %llu\n", numOfSerialReadBufferBytesSaved);
                        // parsing
                        parseSerialPacket();

                        numOfSerialReadBufferBytesSaved = 0;
                        copiedTailChar = '\0';
                        serialReadBuffer[GENERAL_IDENTIFY_BYTE_POSITION] = '\0';
                        ifIdentifyByteMet = false;
                    }
                    else
                    {
                        //NECESSARY_LOG("got a partial packet, size is %llu\n", numOfSerialReadBufferBytesSaved);
                    }
                }
                else
                {
                    //NECESSARY_LOG("got a partial packet, size is %llu\n", numOfSerialReadBufferBytesSaved);
                }
            }
            else
            {
                //NECESSARY_LOG("got a partial packet, size is %llu\n", numOfSerialReadBufferBytesSaved);
            }
        }
    }
}

void PeopleDetection::parseSerialPacket(void)
{
    size_t parsingIndex;
    size_t endingByteIndex;
#if ENABLE_DEBUGGING_PRINTS
    errorReturnIndex_t errorReturnIndex;
#endif // ENABLE_DEBUGGING_PRINTS
    uint8_t protobufDataLen;
    string protobufDataString;
    uint16_t address;
    //int rssi;
    size_t serailReadBufferBytesIndex;
    uint8_t i;
    uint32_t registerStatus;

    serailReadBufferBytesIndex = numOfSerialReadBufferBytesSaved - 1;
#if ENABLE_DEBUGGING_PRINTS
    errorReturnIndex = error_return_index_for_none;
#endif // ENABLE_DEBUGGING_PRINTS
    parsingIndex = GENERAL_IDENTIFY_BYTE_POSITION;

    // Check identify byte
    if (serialReadBuffer[parsingIndex] == CLIENT_IDENTIFY_BYTE)
    {
        parsingIndex = CLIENT_PROTOBUF_DATA_LENGTH_POSITION;
        if (parsingIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_client_protobuf_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        protobufDataLen = static_cast<uint8_t>(serialReadBuffer[parsingIndex]);
        endingByteIndex = CLIENT_PROTOBUF_DATA_START_POSITION + protobufDataLen;
        if (endingByteIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_client_protobuf_data_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        // Check ending byte
        if (serialReadBuffer[endingByteIndex] != GENERAL_BUFFER_END_BYTE)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_end_byte_parsing;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        // NULL terminate
        serialReadBuffer[endingByteIndex] = '\0';
        // Use length assign to avoid '\0' in the information
        protobufDataString.assign(serialReadBuffer + CLIENT_PROTOBUF_DATA_START_POSITION, protobufDataLen);
        // Protobuf unpacking
        if (!clientNotification.ParseFromString(protobufDataString))
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_client_protobuf_data_parsing;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        handleClientNotification();
    }
    else if (serialReadBuffer[parsingIndex] == SERVER_IDENTIFY_BYTE)
    {
        parsingIndex = SERVER_NODE_ADDRESS_PARSING_START_POSITION;
        if (parsingIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_server_address_rssi_buffer_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        address = static_cast<uint16_t>(strtol(static_cast<const char *>(serialReadBuffer+parsingIndex), nullptr, 16));

        parsingIndex = SERVER_NODE_RSSI_START_POSITION;
        if (parsingIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_server_address_rssi_buffer_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }
        //rssi = atoi(static_cast<const char *>((serialReadBuffer+parsingIndex)));

        parsingIndex = SERVER_NODE_PROTOBUF_DATA_LENGTH_POSITION;
        if (parsingIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_server_protobuf_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }
        // rid of the indication char
        protobufDataLen = static_cast<uint8_t>(serialReadBuffer[parsingIndex]) - 1;

        parsingIndex = SERVER_NODE_PROTOBUF_DATA_INDICATION_BYTE_START_POSITION;
        if (parsingIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_server_protobuf_indication_byte_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        // Check ending byte
        endingByteIndex = SERVER_NODE_PROTOBUF_DATA_START_POSITION + protobufDataLen;
        if (endingByteIndex > serailReadBufferBytesIndex)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_server_protobuf_data_len;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        if (serialReadBuffer[endingByteIndex] != GENERAL_BUFFER_END_BYTE)
        {
#if ENABLE_DEBUGGING_PRINTS
            errorReturnIndex = error_return_index_for_end_byte_parsing;
            goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
        }

        // NULL terminate
        serialReadBuffer[endingByteIndex] = '\0';
        // Use length assign to avoid '\0' in the information
        protobufDataString.assign(serialReadBuffer + SERVER_NODE_PROTOBUF_DATA_START_POSITION, protobufDataLen);
        // Check the indication char
        switch (serialReadBuffer[parsingIndex])
        {
            case SERVER_NODE_COLLECTED_DATA_INDICATION_BYTE:
                // Protobuf unpacking
                if (!sensorData.ParseFromString(protobufDataString))
                {
#if ENABLE_DEBUGGING_PRINTS
                    errorReturnIndex = error_return_index_for_server_protobuf_data_parsing;
                    goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
                }

                handleServerSensoredData(address);
                break;
            case SERVER_NODE_POWER_STATUS_INDICATION_BYTE:

                break;
            case SERVER_NODE_TESTING_INDICATION_BYTE:

                break;
            case SERVER_NODE_REGISTER_INDICATION_BYTE:
                if (!sensorRegisterStatus.ParseFromString(protobufDataString))
                {
#if ENABLE_DEBUGGING_PRINTS
                    errorReturnIndex = error_return_index_for_server_protobuf_data_parsing;
                    goto PARSING_ERROR_RETURN;
#endif // ENABLE_DEBUGGING_PRINTS
                }

                registerStatus = sensorRegisterStatus.status();
                CONDITIONAL_LOG(true, "Node(0x%4.4x) is registered as %u\n",
                                address,
                                registerStatus);
                if (registerStatus == 0)
                {
                    QMessageBox msgBox;
                    msgBox.setText("Node ID should not be set with zero!");
                    msgBox.exec();
                }
                else
                {
                    // Check ID conflicts
                    for (i=0; i<MAX_NODE_NUMBER; i++)
                    {
                        if ((nodeDetails[i].type == NODE_TYPE_TWO_ADC) &&
                            (nodeDetails[i].twoADCDetectionPtr != nullptr))
                        {
                            if ((registerStatus == nodeDetails[i].twoADCDetectionPtr->id) &&
                                (address != nodeDetails[i].address))
                            {
                                break;
                            }
                        }
                        else if ((nodeDetails[i].type == NODE_TYPE_FOUR_ADC) &&
                                 (nodeDetails[i].fourADCDetectionPtr != nullptr))
                        {
                            if ((registerStatus == nodeDetails[i].fourADCDetectionPtr->id) &&
                                (address != nodeDetails[i].address))
                            {
                                break;
                            }
                        }
                    }
                    if (i != MAX_NODE_NUMBER)
                    {
                        conflictNodeAddress = address;
                        // conflict happens
                        CONDITIONAL_LOG(true, "Conflict happens for node(0x%4.4x) with id(%u)\n",
                                        address,
                                        registerStatus);
                        QMessageBox msgBox;
                        msgBox.setText(QString().sprintf("Conflict happens for node(0x%4.4x) with id(%u)!",
                                                         address,
                                                         registerStatus));
                        msgBox.exec();
                        // mark as conflict
                        mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NODE_ID_CONFLICT_INDEX] = true;
                    }
                    else
                    {
                        for (i=0; i<MAX_NODE_NUMBER; i++)
                        {
                            if (nodeDetails[i].address == address)
                            {
                                if ((registerStatus >= FOUR_ADC_NODE_ID_LOWER_BOUND) &&
                                    (registerStatus <= FOUR_ADC_NODE_ID_UPPER_BOUND))
                                {
#if ENABLE_ALGORITHM2_CALCULATION
                                    if (nodeDetails[i].fourADCDetectionPtr == nullptr)
                                    {
                                        registeredNodesNum++;
                                        initOKNodesNum++;
                                        nodeDetails[i].type = NODE_TYPE_FOUR_ADC;
                                        //CONDITIONAL_LOG(true, "Node id is %u and type is NODE_TYPE_FOUR_ADC\n", registerStatus);
                                        nodeDetails[i].fourADCDetectionPtr = new fourADCNodeDetection_t();

                                        nodeDetails[i].fourADCDetectionPtr->ifRegistered = true;
                                        nodeDetails[i].fourADCDetectionPtr->id = registerStatus;
                                        nodeDetails[i].fourADCDetectionPtr->ifMotionInOnePeriod = false;
                                        nodeDetails[i].fourADCDetectionPtr->ifCalculationInProgress = false;
                                        nodeDetails[i].fourADCDetectionPtr->ifForcedToCalculate = false;
                                        nodeDetails[i].fourADCDetectionPtr->ifInChillingProcess = false;
                                        nodeDetails[i].fourADCDetectionPtr->ifGPIOHigh = false;

                                        nodeDetails[i].fourADCDetectionPtr->chillTimer = new QTimer(this);
                                        nodeDetails[i].fourADCDetectionPtr->chillTimer->setSingleShot(true);
                                        connect(nodeDetails[i].fourADCDetectionPtr->chillTimer, SIGNAL(timeout()), this, SLOT(algorithm2ChillingTimerTimeout()));

                                        nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer = new QTimer(this);
                                        nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer->setSingleShot(true);
                                        connect(nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer, SIGNAL(timeout()), this, SLOT(algorithm2ForcedCalculationTimerTimeout()));

#if DATA_SAVING_WHILE_COMPUTING
                                        // Create the files
                                        nodeDetails[i].rawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                    mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                    FOUR_ADC_FILE_NAME_MIDDLE_PART,
                                                                                    nodeDetails[i].address,
                                                                                    NODE_RAW_DATA_LOGGING_FILENAME_STRING,
                                                                                    DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                        if (nodeDetails[i].rawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                        {
                                            nodeDetails[i].rawDataLoggingFile.close();
                                        }

                                        nodeDetails[i].algorithmResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                                mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                                FOUR_ADC_FILE_NAME_MIDDLE_PART,
                                                                                                nodeDetails[i].address,
                                                                                                NODE_RESULTS_LOGGING_FILENAME_STRING,
                                                                                                DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                        if (nodeDetails[i].algorithmResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                        {
                                            nodeDetails[i].algorithmResultsLoggingFile.close();
                                        }
#endif // DATA_SAVING_WHILE_COMPUTING

                                        // Update the interaction UI
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].address = address;
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", address));
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_CONNECTED_LABEL_CONTENT);
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_INIT_STATUS_SUCCESS_LABEL_CONTENT);
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                        //connect(mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                    }
#endif // ENABLE_ALGORITHM2_CALCULATION
                                    break;
                                }
                                else if ((registerStatus >= TWO_ADC_NODE_ID_LOWER_BOUND) &&
                                         (registerStatus <= TWO_ADC_NODE_ID_UPPER_BOUND))
                                {
#if ENABLE_ALGORITHM1_CALCULATION
                                    if (nodeDetails[i].twoADCDetectionPtr == nullptr)
                                    {
                                        registeredNodesNum++;

                                        nodeDetails[i].type = NODE_TYPE_TWO_ADC;
                                        //CONDITIONAL_LOG(true, "Node id is %u and type is NODE_TYPE_TWO_ADC\n", registerStatus);
                                        nodeDetails[i].twoADCDetectionPtr = new twoADCNodeDetection_t();
#if ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                                        nodeDetails[i].twoADCDetectionPtr->ifInitialed = false;
#else
                                        nodeDetails[i].twoADCDetectionPtr->ifInitialed = true;
#endif // ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                                        nodeDetails[i].twoADCDetectionPtr->initTimesCounter = 0;
                                        nodeDetails[i].twoADCDetectionPtr->occupancyState = 0;
                                        nodeDetails[i].twoADCDetectionPtr->occupancySwitch = 0;
                                        nodeDetails[i].twoADCDetectionPtr->motionLength = 0;
                                        nodeDetails[i].twoADCDetectionPtr->AState = 0;

                                        nodeDetails[i].twoADCDetectionPtr->ifRegistered = true;
                                        nodeDetails[i].twoADCDetectionPtr->id = registerStatus;

#if DATA_SAVING_WHILE_COMPUTING
                                        // Create the files
                                        nodeDetails[i].rawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                    mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                    TWO_ADC_FILE_NAME_MIDDLE_PART,
                                                                                    nodeDetails[i].address,
                                                                                    NODE_RAW_DATA_LOGGING_FILENAME_STRING,
                                                                                    DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                        if (nodeDetails[i].rawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                        {
                                            nodeDetails[i].rawDataLoggingFile.close();
                                        }

                                        nodeDetails[i].algorithmResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                                mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                                TWO_ADC_FILE_NAME_MIDDLE_PART,
                                                                                                nodeDetails[i].address,
                                                                                                NODE_RESULTS_LOGGING_FILENAME_STRING,
                                                                                                DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                        if (nodeDetails[i].algorithmResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                        {
                                            nodeDetails[i].algorithmResultsLoggingFile.close();
                                        }
#endif // DATA_SAVING_WHILE_COMPUTING

                                        // Update the interaction UI
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].address = address;
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", address));
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_CONNECTED_LABEL_CONTENT);
                                        mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                        connect(mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                    }
#endif // ENABLE_ALGORITHM1_CALCULATION
                                    break;
                                }
                            }
                        }
                        if (i == MAX_NODE_NUMBER)
                        {
                            // wrong order register
                            for (i=0; i<MAX_NODE_NUMBER; i++)
                            {
                                if (nodeDetails[i].address == 0)
                                {
                                    nodeDetails[i].address = address;

                                    if ((registerStatus >= FOUR_ADC_NODE_ID_LOWER_BOUND) &&
                                        (registerStatus <= FOUR_ADC_NODE_ID_UPPER_BOUND))
                                    {
    #if ENABLE_ALGORITHM2_CALCULATION
                                        if (nodeDetails[i].fourADCDetectionPtr == nullptr)
                                        {
                                            registeredNodesNum++;
                                            initOKNodesNum++;
                                            nodeDetails[i].type = NODE_TYPE_FOUR_ADC;
                                            //CONDITIONAL_LOG(true, "Node id is %u and type is NODE_TYPE_FOUR_ADC\n", registerStatus);
                                            nodeDetails[i].fourADCDetectionPtr = new fourADCNodeDetection_t();

                                            nodeDetails[i].fourADCDetectionPtr->ifRegistered = true;
                                            nodeDetails[i].fourADCDetectionPtr->id = registerStatus;
                                            nodeDetails[i].fourADCDetectionPtr->ifMotionInOnePeriod = false;
                                            nodeDetails[i].fourADCDetectionPtr->ifCalculationInProgress = false;
                                            nodeDetails[i].fourADCDetectionPtr->ifForcedToCalculate = false;
                                            nodeDetails[i].fourADCDetectionPtr->ifInChillingProcess = false;
                                            nodeDetails[i].fourADCDetectionPtr->ifGPIOHigh = false;

                                            nodeDetails[i].fourADCDetectionPtr->chillTimer = new QTimer(this);
                                            nodeDetails[i].fourADCDetectionPtr->chillTimer->setSingleShot(true);
                                            connect(nodeDetails[i].fourADCDetectionPtr->chillTimer, SIGNAL(timeout()), this, SLOT(algorithm2ChillingTimerTimeout()));

                                            nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer = new QTimer(this);
                                            nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer->setSingleShot(true);
                                            connect(nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer, SIGNAL(timeout()), this, SLOT(algorithm2ForcedCalculationTimerTimeout()));

    #if DATA_SAVING_WHILE_COMPUTING
                                            // Create the files
                                            nodeDetails[i].rawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                        mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                        FOUR_ADC_FILE_NAME_MIDDLE_PART,
                                                                                        nodeDetails[i].address,
                                                                                        NODE_RAW_DATA_LOGGING_FILENAME_STRING,
                                                                                        DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                            if (nodeDetails[i].rawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                            {
                                                nodeDetails[i].rawDataLoggingFile.close();
                                            }

                                            nodeDetails[i].algorithmResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                                    mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                                    FOUR_ADC_FILE_NAME_MIDDLE_PART,
                                                                                                    nodeDetails[i].address,
                                                                                                    NODE_RESULTS_LOGGING_FILENAME_STRING,
                                                                                                    DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                            if (nodeDetails[i].algorithmResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                            {
                                                nodeDetails[i].algorithmResultsLoggingFile.close();
                                            }
    #endif // DATA_SAVING_WHILE_COMPUTING

                                            // Update the interaction UI
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].address = address;
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", address));
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_CONNECTED_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_INIT_STATUS_SUCCESS_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                            //connect(mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                        }
    #endif // ENABLE_ALGORITHM2_CALCULATION
                                        break;
                                    }
                                    else if ((registerStatus >= TWO_ADC_NODE_ID_LOWER_BOUND) &&
                                             (registerStatus <= TWO_ADC_NODE_ID_UPPER_BOUND))
                                    {
    #if ENABLE_ALGORITHM1_CALCULATION
                                        if (nodeDetails[i].twoADCDetectionPtr == nullptr)
                                        {
                                            registeredNodesNum++;

                                            nodeDetails[i].type = NODE_TYPE_TWO_ADC;
                                            //CONDITIONAL_LOG(true, "Node id is %u and type is NODE_TYPE_TWO_ADC\n", registerStatus);
                                            nodeDetails[i].twoADCDetectionPtr = new twoADCNodeDetection_t();
    #if ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                                            nodeDetails[i].twoADCDetectionPtr->ifInitialed = false;
    #else
                                            nodeDetails[i].twoADCDetectionPtr->ifInitialed = true;
    #endif // ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                                            nodeDetails[i].twoADCDetectionPtr->initTimesCounter = 0;
                                            nodeDetails[i].twoADCDetectionPtr->occupancyState = 0;
                                            nodeDetails[i].twoADCDetectionPtr->occupancySwitch = 0;
                                            nodeDetails[i].twoADCDetectionPtr->motionLength = 0;
                                            nodeDetails[i].twoADCDetectionPtr->AState = 0;

                                            nodeDetails[i].twoADCDetectionPtr->ifRegistered = true;
                                            nodeDetails[i].twoADCDetectionPtr->id = registerStatus;

    #if DATA_SAVING_WHILE_COMPUTING
                                            // Create the files
                                            nodeDetails[i].rawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                        mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                        TWO_ADC_FILE_NAME_MIDDLE_PART,
                                                                                        nodeDetails[i].address,
                                                                                        NODE_RAW_DATA_LOGGING_FILENAME_STRING,
                                                                                        DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                            if (nodeDetails[i].rawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                            {
                                                nodeDetails[i].rawDataLoggingFile.close();
                                            }

                                            nodeDetails[i].algorithmResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                                                    mainWindowPtr->currentDateString.toLatin1().data(),
                                                                                                    TWO_ADC_FILE_NAME_MIDDLE_PART,
                                                                                                    nodeDetails[i].address,
                                                                                                    NODE_RESULTS_LOGGING_FILENAME_STRING,
                                                                                                    DEFAULT_LOGGING_FILE_TYPE_STRING)));
                                            if (nodeDetails[i].algorithmResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
                                            {
                                                nodeDetails[i].algorithmResultsLoggingFile.close();
                                            }
    #endif // DATA_SAVING_WHILE_COMPUTING

                                            // Update the interaction UI
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].address = address;
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", address));
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_CONNECTED_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                            connect(mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                        }
    #endif // ENABLE_ALGORITHM1_CALCULATION
                                        break;
                                    }
                                }
                            }
                            CONDITIONAL_LOG(true, "Wrong order registration is done\n");
                        }
                    }
                }
                break;
        }
    }
#if ENABLE_DEBUGGING_PRINTS
    else
    {
        errorReturnIndex = error_return_index_for_identify_byte_parsing;
        goto PARSING_ERROR_RETURN;
    }
#endif // ENABLE_DEBUGGING_PRINTS
    return;
#if ENABLE_DEBUGGING_PRINTS
PARSING_ERROR_RETURN:
    CONDITIONAL_LOG(ifErrorLoggingOn, "%s\n", serialDataParsingErrorReturnMsgs[errorReturnIndex]);
    return;
#endif // ENABLE_DEBUGGING_PRINTS
}

void PeopleDetection::handleClientNotification()
{
    int i;
    uint16_t address;

    switch(clientNotification.notification_type())
    {
        case Log:
            if (clientNotification.has_node_address())
            {
                address = static_cast<uint16_t>(clientNotification.node_address());
                if (clientNotification.has_nwk_status())
                {
                    for (i=0; i<MAX_NODE_NUMBER; i++)
                    {
                        if (nodeDetails[i].address == address)
                        {
                            break;
                        }
                    }

                    switch(clientNotification.nwk_status())
                    {
                        case nwk_on:
                            if (i == MAX_NODE_NUMBER)
                            {
                                for (i=0; i<MAX_NODE_NUMBER; i++)
                                {
                                    if (nodeDetails[i].address == 0)
                                    {
                                        nodeDetails[i].address = address;
                                        break;
                                    }
                                }
                            }
                            CONDITIONAL_LOG(true, "The node(0x%4.4x) is in on state.\n", address);
                            break;
                        case nwk_off:
                            if (i != MAX_NODE_NUMBER)
                            {
                                // Update connection status
                                switch (nodeDetails[i].type)
                                {
                                    case NODE_TYPE_TWO_ADC:
#if ENABLE_ALGORITHM1_CALCULATION
                                        if (nodeDetails[i].twoADCDetectionPtr != nullptr)
                                        {
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_DISCONNECTED_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].address = DEFAULT_NODE_ADDRESS;
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", DEFAULT_NODE_ADDRESS));
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                            if (nodeDetails[i].twoADCDetectionPtr->ifRegistered)
                                            {
                                                registeredNodesNum--;
                                                nodeDetails[i].twoADCDetectionPtr->ifRegistered = false;
                                            }

                                            if (nodeDetails[i].twoADCDetectionPtr->ifInitialed)
                                            {
                                                initOKNodesNum--;
                                            }
                                            // Update the interaction UI

                                            disconnect(mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                            nodeDetails[i].twoADCDetectionPtr->id = 0;

                                            //nodeDetails[i].address = 0;
                                            delete nodeDetails[i].twoADCDetectionPtr;
                                            nodeDetails[i].twoADCDetectionPtr = nullptr;
                                        }
#endif // ENABLE_ALGORITHM1_CALCULATION
                                        break;
                                    case NODE_TYPE_FOUR_ADC:
#if ENABLE_ALGORITHM2_CALCULATION
                                        if (nodeDetails[i].fourADCDetectionPtr != nullptr)
                                        {
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].connectionStatusLabel->setText(CONNECTION_STATUS_DISCONNECTED_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].address = DEFAULT_NODE_ADDRESS;
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].addressLabel->setText(QString().sprintf("0x%4.4x", DEFAULT_NODE_ADDRESS));
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NULL);

                                            if (nodeDetails[i].fourADCDetectionPtr->ifRegistered)
                                            {
                                                registeredNodesNum--;
                                                nodeDetails[i].fourADCDetectionPtr->ifRegistered = false;
                                            }

                                            initOKNodesNum--;
                                            // Update the interaction UI

                                            //disconnect(mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id-1].resetPushButton, SIGNAL(clicked()), this, SLOT(nodeDetectionInit()));
                                            nodeDetails[i].fourADCDetectionPtr->id = 0;

                                            //nodeDetails[i].address = 0;
                                            delete nodeDetails[i].fourADCDetectionPtr;
                                            nodeDetails[i].fourADCDetectionPtr = nullptr;
                                        }
#endif // ENABLE_ALGORITHM2_CALCULATION
                                        break;
                                    default:
                                        break;
                                }
                                nodeDetails[i].type = NODE_TYPE_NULL;

                                if (conflictNodeAddress != 0)
                                {
                                    if (address == conflictNodeAddress)
                                    {
                                        // mark as conflict
                                        mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NODE_ID_CONFLICT_INDEX] = false;
                                        CONDITIONAL_LOG(true, "Conflict is released for node(0x%4.4x)\n",
                                                        address);
                                        conflictNodeAddress = 0;
                                    }
                                }
                            }
                            CONDITIONAL_LOG(true, "The node(0x%4.4x) is in off state.\n", address);
                            break;
                        case nwk_char_discovery_done:
                            CONDITIONAL_LOG(true, "The node(0x%4.4x) is done with char discovery.\n", address);
                            break;
                    }
                }
            }
            break;
        case Assert:
            CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "This is an assertion message.\n");
            if (clientNotification.has_extended_msg())
            {
                CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "The assertion is %s.\n", clientNotification.extended_msg().c_str());
            }
            break;
        case HeartBeat:
            //CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "This is a heart-beat message.\n");
            mainWindowPtr->serialConnectionStatusLabel->setText(SERIAL_CONNECTION_STATUS_GOOD_CONTENT);
            mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN_INDEX] = false;
            serialConnectionMonitorTimer->start(SERIAL_CONNECTION_MONITOR_TIMEOUT_MS);
            break;
        case Sensor:
#if ENABLE_ALGORITHM_CALCULATION
            //CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "This is a sensor message %u.\n", clientNotification.gpio_status());
            if (clientNotification.has_gpio_status())
            {
                vecClientGPIOStatus.push_back(clientNotification.gpio_status());
            }
#endif // ENABLE_ALGORITHM_CALCULATION
            break;
    }
}

void PeopleDetection::handleServerSensoredData(uint16_t address)
{
    int i, j;
    int nodeIndex = 0;
    char loggingBuffer[FILE_LOGGING_BUFFER_SIZE_BYTES];
    int sprintfIndex;
    int sprintfIndexConstant;
    int symbolIndex;

    for (i=0; i<MAX_NODE_NUMBER; i++)
    {
        if (nodeDetails[i].address == address)
        {
            nodeIndex = i;
            break;
        }
    }

    if (i == MAX_NODE_NUMBER)
    {
        return;
    }

#if ENABLE_ALGORITHM_CALCULATION
#if ENABLE_ALGORITHM1_CALCULATION
    if ((nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC) &&
        (nodeDetails[nodeIndex].twoADCDetectionPtr != nullptr) &&
        (nodeDetails[nodeIndex].twoADCDetectionPtr->ifRegistered == false))
    {
        return;
    }
#endif // ENABLE_ALGORITHM1_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
    if ((nodeDetails[nodeIndex].type == NODE_TYPE_FOUR_ADC) &&
        (nodeDetails[nodeIndex].fourADCDetectionPtr != nullptr) &&
        (nodeDetails[nodeIndex].fourADCDetectionPtr->ifRegistered == false))
    {
        return;
    }
#endif // ENABLE_ALGORITHM2_CALCULATION
#endif // ENABLE_ALGORITHM_CALCULATION

    sprintfIndexConstant = sprintf(loggingBuffer, "%s,", QTime::currentTime().toString("hh:mm:ss").toLatin1().data());
    symbolIndex = sprintfIndexConstant;

    switch(sensorData.type())
    {
        case Normal_Start:
        case Normal_End:
        case Normal_Unconcerned:
            if (sensorData.type() == Normal_Start)
            {
                if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                {
                    loggingBuffer[sprintfIndexConstant++] = START_FRAME_DATA_SYMBOL;
                    loggingBuffer[sprintfIndexConstant++] = ',';
                }

#if ENABLE_ALGORITHM_CALCULATION
                switch (nodeDetails[nodeIndex].type)
                {
                    case NODE_TYPE_TWO_ADC:
#if ENABLE_ALGORITHM1_CALCULATION
                        if ((nodeDetails[nodeIndex].twoADCDetectionPtr != nullptr) &&
                            (nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.size() >= TWO_ADC_FRAME_LINE_CONTENT_MIN_NUMBER))
                        {
                            if (sensorData.has_temp() && (nodeDetails[nodeIndex].twoADCDetectionPtr->vecPeriodTemperature.size() == 0))
                            {
                                nodeDetails[nodeIndex].twoADCDetectionPtr->vecPeriodTemperature.append(sensorData.temp());
                            }

                            CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "\n############ %s:Start of 2ADC node 0x%4.4x ############\n",
                                                QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                                                address);
                            // Calculate
                            algorithm1GetPeakPeakDataAndMore(nodeDetails[nodeIndex]);
                            //CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "End of algorithm1GetPeakPeakDataAndMore\n");
                            if (nodeDetails[nodeIndex].twoADCDetectionPtr->ifInitialed)
                            {
                                if (nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults.size() == 2)
                                {
                                    //algorithm1DoAlgorithmComputation(nodeDetails[nodeIndex].twoADCDetectionPtr);
                                    algorithm1DoAlgorithmComputation(nodeDetails[nodeIndex]);
                                    //CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "End of algorithm1DoAlgorithmComputation\n");
                                    nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults.erase(nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults.begin());
                                }
                            }
                            else
                            {
                                if (nodeDetails[nodeIndex].twoADCDetectionPtr->initTimesCounter <= TWO_ADC_NODE_FORCED_INIT_MAX_TIMES)
                                {
                                    nodeDetails[nodeIndex].twoADCDetectionPtr->initTimesCounter++;
                                    nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis.append(nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults[0]);
                                    if (nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis.size() == TWO_ADC_NODE_FORCED_INIT_ELEMENTS_NUMBER)
                                    {
                                        float absVpp0Difference_1_2 = abs(nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[0].adc0PeakPeakVoltage - nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[1].adc0PeakPeakVoltage);
                                        float absVpp1Difference_1_2 = abs(nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[0].adc1PeakPeakVoltage - nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[1].adc1PeakPeakVoltage);
                                        float absVpp0Difference_2_3 = abs(nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[1].adc0PeakPeakVoltage - nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[2].adc0PeakPeakVoltage);
                                        float absVpp1Difference_2_3 = abs(nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[1].adc1PeakPeakVoltage - nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[2].adc1PeakPeakVoltage);

                                        if ((absVpp0Difference_1_2 < INIT_FILTER_VPP_THRESHOLD_VALUE) &&
                                            (absVpp1Difference_1_2 < INIT_FILTER_VPP_THRESHOLD_VALUE) &&
                                            (absVpp0Difference_2_3 < INIT_FILTER_VPP_THRESHOLD_VALUE) &&
                                            (absVpp1Difference_2_3 < INIT_FILTER_VPP_THRESHOLD_VALUE))
                                        {
                                            if ((nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[0].ifExistMotion == 0) &&
                                                (nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[1].ifExistMotion == 0) &&
                                                (nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis[2].ifExistMotion == 0))
                                            {
                                                // Init with the value
                                                NECESSARY_LOG("Init successfully\n");
                                                initOKNodesNum++;
                                                // Add one for init
                                                nodeDetails[nodeIndex].twoADCDetectionPtr->occupancyStateSaved.append(0);

                                                nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResultsA.append(nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults[0]);
                                                nodeDetails[nodeIndex].twoADCDetectionPtr->ifInitialed = true;
                                                mainWindowPtr->InteractionNodes[nodeDetails[nodeIndex].twoADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_INIT_STATUS_SUCCESS_LABEL_CONTENT);

                                                #if DATA_SAVING_WHILE_COMPUTING
                                                if (nodeDetails[nodeIndex].rawDataLoggingFile.open(QIODevice::Append | QIODevice::Text))
                                                {
                                                    for (i=0; i<nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.size(); i++)
                                                    {
                                                        nodeDetails[nodeIndex].rawDataLoggingFile.write(nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.at(i).toLatin1().data());
                                                    }
                                                    nodeDetails[nodeIndex].rawDataLoggingFile.close();
                                                }
                                                #endif // DATA_SAVING_WHILE_COMPUTING
                                            }
                                        }

                                        if (nodeDetails[nodeIndex].twoADCDetectionPtr->ifInitialed == false)
                                        {
                                            nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis.erase(nodeDetails[nodeIndex].twoADCDetectionPtr->vecInitFrameAnalysis.begin());
                                        }
                                    }

                                    if (nodeDetails[nodeIndex].twoADCDetectionPtr->ifInitialed == false)
                                    {
                                        nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults.erase(nodeDetails[nodeIndex].twoADCDetectionPtr->vecFrameAnalysisResults.begin());
                                    }
                                }
                                else
                                {
                                    NECESSARY_LOG("Init failed as timeout\n");
                                    mainWindowPtr->InteractionNodes[nodeDetails[nodeIndex].twoADCDetectionPtr->id-1].resetStatusLabel->setText(CONNECTION_STATUS_INIT_STATUS_FAILED_LABEL_CONTENT);
                                }
                            }
                            CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "############ %s:End   of 2ADC node 0x%4.4x ############\n\n",
                                                QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                                                address);
                            // Clear the size
                            nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.clear();
                        }
#endif // ENABLE_ALGORITHM1_CALCULATION
                        break;
                    default:
                        break;
                }
#endif // ENABLE_ALGORITHM_CALCULATION
                //CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "Normal_Start\n");
            }
            else
            {
                if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                {
                    loggingBuffer[sprintfIndexConstant++] = WORKING_DATA_SYMBOL;
                    loggingBuffer[sprintfIndexConstant++] = ',';
                }
            }

            //CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "Normal\n");
            if (sensorData.has_pwm_status())
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "PWM is on: %d, ", (sensorData.pwm_status())?(1):(0));
                if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                {
                    sprintfIndexConstant += sprintf(loggingBuffer+sprintfIndexConstant, "%d,", (sensorData.pwm_status())?(1):(0));
                }
            }
            else
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "PWM is off| ");
            }

            // temp
            if (sensorData.has_temp())
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "temp is on: %.2f, ",  static_cast<double>(sensorData.temp()));
                if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                {
                    sprintfIndexConstant += sprintf(loggingBuffer+sprintfIndexConstant, "%.2f,", static_cast<double>(sensorData.temp()));
                }
            }
            else
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "temp is off| ");
            }

#if ENABLE_HUMIDITY_PARSING
            // humidity
            if (sensorData.has_humidity())
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "humidity is on: %.2f", static_cast<double>(sensorData.humidity()));
                if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                {
                    sprintfIndexConstant += sprintf(loggingBuffer+sprintfIndexConstant, "%.2f,", static_cast<double>(sensorData.humidity()));
                }
            }
            else
            {
                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "humidity is off| ");
            }
#endif // ENABLE_HUMIDITY_PARSING
            CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "\n");

            if (sensorData.element_size())
            {
                for (i = 0; i< sensorData.element_size(); i++)
                {
                    if ((i!=0) && (sensorData.type() == Normal_Start))
                    {
                        loggingBuffer[symbolIndex] = WORKING_DATA_SYMBOL;
                    }
                    sprintfIndex = sprintfIndexConstant;
                    // ADC
                    if (sensorData.element(i).adc_values_size())
                    {
                        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "Node(0x%4.4x): ", address);
                        if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                        {
                            for (j=0; j<sensorData.element(i).adc_values_size(); j++)
                            {
                                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "ADC[%d] = %.*f, ", j, floatPrecision,  static_cast<double>(sensorData.element(i).adc_values(j)));
                                sprintfIndex += sprintf(loggingBuffer+sprintfIndex, "%.*f,", floatPrecision,  static_cast<double>(sensorData.element(i).adc_values(j)));
                            }
                        }
                        else if (nodeDetails[nodeIndex].type == NODE_TYPE_FOUR_ADC)
                        {
                            for (j=0; j<sensorData.element(i).adc_values_size(); j++)
                            {
                                CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "ADC[%d] = %.*f, ", j, floatPrecision,  static_cast<double>(sensorData.element(i).adc_values(j)));
                                if (j != (sensorData.element(i).adc_values_size() - 1))
                                {
                                    sprintfIndex += sprintf(loggingBuffer+sprintfIndex, "%.*f,", floatPrecision,  static_cast<double>(sensorData.element(i).adc_values(j)));
                                }
                                else
                                {
                                    sprintfIndex += sprintf(loggingBuffer+sprintfIndex, "%.*f\n", floatPrecision,  static_cast<double>(sensorData.element(i).adc_values(j)));
                                }
                            }
                        }
                        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, " ");
                    }
                    else
                    {
                        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "ADC is off| ");
                    }

                    // gpio
                    if (sensorData.element(i).has_gpio())
                    {
                        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "GPIO is on: %d\n", ((sensorData.element(i).gpio() == true) ? (1):(0)));
                        if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
                        {
                            sprintfIndex += sprintf(loggingBuffer+sprintfIndex, "%d\n", ((sensorData.element(i).gpio() == true) ? (1):(0)));
                        }

                        if (sensorData.element(i).gpio())
                        {
                            overallResetTimer->start();
                        }
                    }
                    else
                    {
                        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "gpio is off\n");
                    }

                    if (loggingBuffer[sprintfIndex-1] != '\n')
                    {
                        loggingBuffer[sprintfIndex-1] = '\n';
                    }
#if ENABLE_ALGORITHM_CALCULATION
                    switch (nodeDetails[nodeIndex].type)
                    {
                        case NODE_TYPE_TWO_ADC:
#if ENABLE_ALGORITHM1_CALCULATION
                            if (nodeDetails[nodeIndex].twoADCDetectionPtr != nullptr)
                            {
                                nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.append(QString(loggingBuffer));
                            }
#endif // ENABLE_ALGORITHM1_CALCULATION
                            break;
                        case NODE_TYPE_FOUR_ADC:
#if ENABLE_ALGORITHM2_CALCULATION
                            if (nodeDetails[nodeIndex].fourADCDetectionPtr != nullptr)
                            {
                                if (!nodeDetails[nodeIndex].fourADCDetectionPtr->ifInChillingProcess)
                                {
                                    if (nodeDetails[nodeIndex].fourADCDetectionPtr->ifGPIOHigh)
                                    {
                                        if (sensorData.element(i).gpio())
                                        {
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.append(QString(loggingBuffer));
                                            for (j=0; j<sensorData.element(i).adc_values_size(); j++)
                                            {
                                                nodeDetails[nodeIndex].fourADCDetectionPtr->vecPIRDataArray[j].append(sensorData.element(i).adc_values(j));
                                            }
                                        }

                                        if ((!sensorData.element(i).gpio()) ||
                                            (nodeDetails[nodeIndex].fourADCDetectionPtr->ifForcedToCalculate))
                                        {
                                            // perform one calculation
                                            CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "\n############ %s:Start of 4ADC node 0x%4.4x ############\n",
                                                                QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                                                                address);
                                            algorithm2GetDirectionResult(nodeDetails[nodeIndex]);
                                            // Chill for 5 seconds, trigger the chill timer
                                            CONDITIONAL_LOG(ifConditionalMiscLoggingOn, "############ %s:End   of 4ADC node 0x%4.4x ############\n\n",
                                                                QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                                                                address);

                                            nodeDetails[nodeIndex].fourADCDetectionPtr->chillTimer->start(CHILLING_TIMER_TIMEOUT_MS);
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifInChillingProcess = true;

                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifGPIOHigh = false;
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifCalculationInProgress = false;

                                            nodeDetails[nodeIndex].fourADCDetectionPtr->forcedCalculationTimer->stop();
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifForcedToCalculate = false;
                                        }
                                    }
                                    else
                                    {
                                        if (sensorData.element(i).gpio())
                                        {
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.append(QString(loggingBuffer));
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifMotionInOnePeriod = true;
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifCalculationInProgress = true;
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifGPIOHigh = true;
                                            for (j=0; j<sensorData.element(i).adc_values_size(); j++)
                                            {
                                                nodeDetails[nodeIndex].fourADCDetectionPtr->vecPIRDataArray[j].append(sensorData.element(i).adc_values(j));
                                            }

                                            // Forced 10 seconds calculation
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->forcedCalculationTimer->start(FORCED_CALCULATION_TIMER_TIMEOUT_MS);
                                            nodeDetails[nodeIndex].fourADCDetectionPtr->ifForcedToCalculate = false;
                                        }
                                    }
                                }
                            }
#endif // ENABLE_ALGORITHM2_CALCULATION
                            break;
                        default:
                            break;
                    }
#endif // ENABLE_ALGORITHM_CALCULATION
                }
                // Saved to the temp file
#if ENABLE_ALGORITHM_CALCULATION
#if ENABLE_ALGORITHM2_CALCULATION
                if ((nodeDetails[nodeIndex].type == NODE_TYPE_FOUR_ADC) &&
                    (nodeDetails[nodeIndex].fourADCDetectionPtr != nullptr))
                {
                #if DATA_SAVING_WHILE_COMPUTING
                    if (nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.size() > 0)
                    {
                        if (nodeDetails[nodeIndex].rawDataLoggingFile.open(QIODevice::Append | QIODevice::Text))
                        {
                            for (i = 0; i<nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.size(); i++)
                            {
                                nodeDetails[nodeIndex].rawDataLoggingFile.write(nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.at(i).toLatin1().data());
                            }
                            nodeDetails[nodeIndex].rawDataLoggingFile.close();
                        }
                        nodeDetails[nodeIndex].fourADCDetectionPtr->linesContent.clear();
                    }
                #endif // DATA_SAVING_WHILE_COMPUTING
                }
#endif // ENABLE_ALGORITHM2_CALCULATION
#endif // ENABLE_ALGORITHM_CALCULATION
            }
            break;
        case Interrupt:
            loggingBuffer[sprintfIndexConstant++] = SLEEPING_INTERRUPT_DATA_SYMBOL;
            loggingBuffer[sprintfIndexConstant++] = '\n';
            loggingBuffer[sprintfIndexConstant] = '\0';
#if ENABLE_ALGORITHM_CALCULATION
            if (nodeDetails[nodeIndex].type == NODE_TYPE_TWO_ADC)
            {
#if ENABLE_ALGORITHM1_CALCULATION
                nodeDetails[nodeIndex].twoADCDetectionPtr->linesContent.append(QString(loggingBuffer));
#endif // ENABLE_ALGORITHM1_CALCULATION
            }
#endif // ENABLE_ALGORITHM_CALCULATION
            CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "Interrupt\n");
            break;
    }
}

#if ENABLE_ALGORITHM_CALCULATION
#if ENABLE_ALGORITHM1_CALCULATION
int PeopleDetection::algorithm1GetMaxDataIndexFromRange(QVector<float> &data, int startIndex, int endIndex)
{
    int i;
    int index = startIndex;
    float maxData = data.at(index);

    for (i=(startIndex + 1); i<endIndex; i++)
    {
        if (data.at(i) > maxData)
        {
            maxData = data.at(i);
            index = i;
        }
    }

    return index;
}

int PeopleDetection::algorithm1GetMinDataIndexFromRange(QVector<float> &data, int startIndex, int endIndex)
{
    int i;
    int index = startIndex;
    float minData = data.at(index);

    for (i=(startIndex + 1); i<endIndex; i++)
    {
        if (data.at(i) < minData)
        {
            minData = data.at(i);
            index = i;
        }
    }

    return index;
}

float PeopleDetection::algorithm1ComputePeakPeakVoltage(QVector<float> &data, int startIndex, int endIndex)
{
    float PPvalue = 0.0;
    int maxIndex = algorithm1GetMaxDataIndexFromRange(data, startIndex, endIndex);
    int minIndex = algorithm1GetMinDataIndexFromRange(data, startIndex, endIndex);
#if 0
    int maxIndex1 = algorithm1GetMaxDataIndexFromRange(data, (startIndex+1), 60);
    int minIndex1 = algorithm1GetMinDataIndexFromRange(data, (startIndex+1), 60);

    if (maxIndex1 > minIndex1)
    {
        PPvalue = data.at(maxIndex) - data.at(minIndex);
    }
    else
    {
        PPvalue = -data.at(maxIndex) + data.at(minIndex);
    }
#else
    PPvalue = abs(data.at(maxIndex) - data.at(minIndex));
#endif

    if (abs(PPvalue) < PEAK_PEAK_VALUE_TOO_SMALL_THRESHOLD)
    {
        PPvalue = abs(PPvalue);
    }

    return PPvalue;
}

void PeopleDetection::algorithm1GetPeakPeakDataAndMore(nodeDetail_t &nodeDetail)
{
    int i, j;   // calculation not start from 'N'
    int startIndex = 0;
    int endIndex = nodeDetail.twoADCDetectionPtr->linesContent.size();
    QVector<uint> vecDataMotion;
    QVector<float> vecDataSeg1;
    QVector<float> vecDataSeg2;
    float PPv1;
    float PPv2;
    uint dataMotionValue;

    oneFrameAnalysisResult_t oneFrameAnalysisResult;

    vecDataMotion.clear();
    vecDataSeg1.clear();
    vecDataSeg2.clear();

    oneFrameAnalysisResult.interruptCounter = 0;
    oneFrameAnalysisResult.gpioMotionCounter = 0;
    oneFrameAnalysisResult.ifExistMotion = 0;

#if DATA_SAVING_WHILE_COMPUTING
    if (nodeDetail.twoADCDetectionPtr->ifInitialed)
    {
        nodeDetail.rawDataLoggingFile.open(QIODevice::Append | QIODevice::Text);
    }
#endif // DATA_SAVING_WHILE_COMPUTING
    for (i=startIndex, j=(startIndex+1); i<(endIndex-1); i++, j++)
    {
    #if DATA_SAVING_WHILE_COMPUTING
        if (nodeDetail.twoADCDetectionPtr->ifInitialed && nodeDetail.rawDataLoggingFile.isOpen())
        {
            nodeDetail.rawDataLoggingFile.write(nodeDetail.twoADCDetectionPtr->linesContent.at(i).toLatin1().data());
        }
    #endif // DATA_SAVING_WHILE_COMPUTING
        QStringList splitData = nodeDetail.twoADCDetectionPtr->linesContent.at(j).split(CSV_SPLIT_SYMBOL);
        if (splitData.size() == CSV_FILE_WORKING_MODE_SPLIT_LIST_SIZE)
        {
            dataMotionValue = splitData.at(CSV_FILE_MOTION_DATA_COLUMN_INDEX).toUInt();
            if (dataMotionValue == 1)
            {
                oneFrameAnalysisResult.gpioMotionCounter++;
            }
            vecDataSeg1.push_back(splitData.at(CSV_FILE_ADC0_DATA_COLUMN_INDEX).toFloat());
            vecDataSeg2.push_back(splitData.at(CSV_FILE_ADC1_DATA_COLUMN_INDEX).toFloat());
        }
        else if (splitData.size() == CSV_FILE_SLEEPING_MODE_SPLIT_LIST_SIZE)
        {
            oneFrameAnalysisResult.interruptCounter++;
        }
    }
#if DATA_SAVING_WHILE_COMPUTING
    if (nodeDetail.twoADCDetectionPtr->ifInitialed && nodeDetail.rawDataLoggingFile.isOpen())
    {
        nodeDetail.rawDataLoggingFile.close();
    }
#endif // DATA_SAVING_WHILE_COMPUTING
    PPv1 = algorithm1ComputePeakPeakVoltage(vecDataSeg1, 0, vecDataSeg1.size());
    PPv2 = algorithm1ComputePeakPeakVoltage(vecDataSeg2, 0, vecDataSeg2.size());

    if ((oneFrameAnalysisResult.gpioMotionCounter == 0)
        && (oneFrameAnalysisResult.interruptCounter == 0))
    {
        oneFrameAnalysisResult.ifExistMotion = 0;
    }
    else
    {
        oneFrameAnalysisResult.ifExistMotion = 1;
    }

    oneFrameAnalysisResult.adc0PeakPeakVoltage = PPv1;
    oneFrameAnalysisResult.adc1PeakPeakVoltage = PPv2;
    for (i=(startIndex+1); i<endIndex; i++)
    {
        QStringList splitData = nodeDetail.twoADCDetectionPtr->linesContent.at(i).split(CSV_SPLIT_SYMBOL);
        if (splitData.size() == CSV_FILE_WORKING_MODE_SPLIT_LIST_SIZE)
        {
            oneFrameAnalysisResult.currentTemperature = splitData.at(CSV_FILE_TEMPERATURE_DATA_COLUMN_INDEX).toFloat();
            break;
        }
    }

    nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResults.push_back(oneFrameAnalysisResult);
    NECESSARY_LOG("Frame result: [%7.*f, %7.*f, %.2f, %3d, %d, %d]\n",
           floatPrecision,
           oneFrameAnalysisResult.adc0PeakPeakVoltage,
           floatPrecision,
           oneFrameAnalysisResult.adc1PeakPeakVoltage,
           oneFrameAnalysisResult.currentTemperature,
           oneFrameAnalysisResult.gpioMotionCounter,
           oneFrameAnalysisResult.interruptCounter,
           oneFrameAnalysisResult.ifExistMotion);
}

//void PeopleDetection::algorithm1DoAlgorithmComputation(twoADCNodeDetection_t *twoADCDetectionPtr)
void PeopleDetection::algorithm1DoAlgorithmComputation(nodeDetail_t &nodeDetail)
{
    int motionStatusOld = nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResults[0].ifExistMotion;
    int motionStatusCurrent = nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResults[1].ifExistMotion;
    float vppDiff1;
    float vppDiff2;
#if ENABLE_DEBUGGING_PRINTS
    float tempDiff;
#endif // ENABLE_DEBUGGING_PRINTS
    float thresholdOccupancySwitch = DEFAULT_THRESHOLD_OCCUPANCY_SWITCH_SCALE;
    int i;

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "motion_status_current: %d motion_status_old: %d\n",
                        motionStatusCurrent,
                        motionStatusOld);
    if ((motionStatusCurrent == 0) && (motionStatusOld == 0))
    {
        if (nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.size())
        {
            nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.erase(nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.begin());
        }
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.append(nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResults[1]);
        nodeDetail.twoADCDetectionPtr->AState = nodeDetail.twoADCDetectionPtr->occupancyState;
        nodeDetail.twoADCDetectionPtr->occupancySwitch = 0;
        nodeDetail.twoADCDetectionPtr->motionLength += 1;
        if (nodeDetail.twoADCDetectionPtr->motionLength > DEFAULT_THRESHOLD_NON_MOTION_LENGTH)
        {
            nodeDetail.twoADCDetectionPtr->occupancyState = 0;
        }
        nodeDetail.twoADCDetectionPtr->occupancyStateSaved.append(nodeDetail.twoADCDetectionPtr->occupancyState);
        CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "Occupancy state switch: %d occupancy_state: %d\n",
                            nodeDetail.twoADCDetectionPtr->occupancySwitch,
                            nodeDetail.twoADCDetectionPtr->occupancyState);
    }
    else if ((motionStatusCurrent == 1) && (motionStatusOld == 0))
    {
        nodeDetail.twoADCDetectionPtr->motionLength = 0;
        nodeDetail.twoADCDetectionPtr->occupancySwitch = -1;
        nodeDetail.twoADCDetectionPtr->occupancyState = 1;
        nodeDetail.twoADCDetectionPtr->occupancyStateSaved.append(nodeDetail.twoADCDetectionPtr->occupancyState);
        CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "Occupancy state switch: %d occupancy_state: %d\n",
               nodeDetail.twoADCDetectionPtr->occupancySwitch,
               nodeDetail.twoADCDetectionPtr->occupancyState);
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB.clear();
    }
    else if ((motionStatusCurrent == 1) && (motionStatusOld == 1))
    {
        nodeDetail.twoADCDetectionPtr->occupancyState = 1;
        nodeDetail.twoADCDetectionPtr->occupancyStateSaved.append(nodeDetail.twoADCDetectionPtr->occupancyState);
        nodeDetail.twoADCDetectionPtr->occupancySwitch = 0;
        CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "Occupancy state switch: %d occupancy_state: %d\n",
               nodeDetail.twoADCDetectionPtr->occupancySwitch,
               nodeDetail.twoADCDetectionPtr->occupancyState);
    }
    else if ((motionStatusCurrent == 0) && (motionStatusOld == 1))
    {
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB.append(nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResults[1]);
#if !ENABLE_ALGORITHM1_FORCED_INITIALIZATION
        if (nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.size() == 0)
        {
            nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.append(nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0]);
            nodeDetail.twoADCDetectionPtr->AState = nodeDetail.twoADCDetectionPtr->occupancyState;
            nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB.clear();
            return;
        }
#endif // ENABLE_ALGORITHM1_FORCED_INITIALIZATION
        CONDITIONAL_LOG(true, "A: [%.2f, %.2f, %.2f, %d, %d, %d] A state: %d\n",
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].adc0PeakPeakVoltage,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].adc1PeakPeakVoltage,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].currentTemperature,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].gpioMotionCounter,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].interruptCounter,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].ifExistMotion,
                nodeDetail.twoADCDetectionPtr->AState);
        CONDITIONAL_LOG(true, "B: [%.2f, %.2f, %.2f, %d, %d, %d]\n",
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].adc0PeakPeakVoltage,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].adc1PeakPeakVoltage,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].currentTemperature,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].gpioMotionCounter,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].interruptCounter,
                nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].ifExistMotion);
        vppDiff1 = nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].adc0PeakPeakVoltage - nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].adc0PeakPeakVoltage;
        vppDiff2 = nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].adc1PeakPeakVoltage - nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].adc1PeakPeakVoltage;
#if ENABLE_DEBUGGING_PRINTS
        tempDiff = nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0].currentTemperature - nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA[0].currentTemperature;
#endif // ENABLE_DEBUGGING_PRINTS
        CONDITIONAL_LOG(ifDynamicSamplingLoggingOn, "Vpp_change[V]: %.3f %.3f Temp_change[C]: %.3f\n",
               vppDiff1,
               vppDiff2,
               tempDiff);
        if (abs(vppDiff1 + vppDiff2) > thresholdOccupancySwitch)
        {
            nodeDetail.twoADCDetectionPtr->occupancySwitch = 1;
        }
        else
        {
            nodeDetail.twoADCDetectionPtr->occupancySwitch = 0;
        }

        if (nodeDetail.twoADCDetectionPtr->occupancySwitch == 1)
        {
            nodeDetail.twoADCDetectionPtr->occupancyState = 1 - nodeDetail.twoADCDetectionPtr->AState;
            nodeDetail.twoADCDetectionPtr->occupancyStateSaved.append(nodeDetail.twoADCDetectionPtr->occupancyState);
        }
        else if (nodeDetail.twoADCDetectionPtr->occupancySwitch == 0)
        {
            nodeDetail.twoADCDetectionPtr->occupancyState = nodeDetail.twoADCDetectionPtr->AState;
            nodeDetail.twoADCDetectionPtr->occupancyStateSaved.append(nodeDetail.twoADCDetectionPtr->occupancyState);
        }

        CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "Occupancy state switch: %d occupancy_state: %d\n",
               nodeDetail.twoADCDetectionPtr->occupancySwitch,
               nodeDetail.twoADCDetectionPtr->occupancyState);
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.clear();
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsA.append(nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB[0]);
        nodeDetail.twoADCDetectionPtr->AState = nodeDetail.twoADCDetectionPtr->occupancyState;
        nodeDetail.twoADCDetectionPtr->vecFrameAnalysisResultsB.clear();
    }

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "occupancy_state_saved: [");
    for (i=0; i<nodeDetail.twoADCDetectionPtr->occupancyStateSaved.size(); i++)
    {
        CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "%d, ", nodeDetail.twoADCDetectionPtr->occupancyStateSaved.at(i));
    }
    CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "]\n");

    if (nodeDetail.twoADCDetectionPtr->occupancyStateSaved.size())
    {
        nodeDetail.twoADCDetectionPtr->occupancyStateSaved.erase(nodeDetail.twoADCDetectionPtr->occupancyStateSaved.begin());
    }

#if DATA_SAVING_WHILE_COMPUTING
    if (nodeDetail.algorithmResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
    {
        nodeDetail.algorithmResultsLoggingFile.write(
                    QString().sprintf("%s,%d\n",
                    QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                    nodeDetail.twoADCDetectionPtr->occupancyState).toLatin1().data());
        nodeDetail.algorithmResultsLoggingFile.close();
    }
#endif // DATA_SAVING_WHILE_COMPUTING
}
#endif // ENABLE_ALGORITHM1_CALCULATION

#if ENABLE_ALGORITHM2_CALCULATION
void PeopleDetection::algorithm2DetectPeak(nodeDetail_t &nodeDetail, int ADCIndex)
{
    int i;
    float absDifference;
    int PIRDataVectorSize = nodeDetail.fourADCDetectionPtr->vecPIRDataArray[ADCIndex].size();
    QVector<int> vecDir(PIRDataVectorSize, 0);

    nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].clear();
    for (i=1; i<PIRDataVectorSize; i++)
    {
        if (nodeDetail.fourADCDetectionPtr->vecPIRDataArray[ADCIndex].at(i) >= nodeDetail.fourADCDetectionPtr->vecPIRDataArray[ADCIndex].at((i-1)))
        {
            vecDir[i] = 1;
        }
        else
        {
            vecDir[i] = -1;
        }

        if (i>0)
        {
            if (((vecDir[i] == -1) && (vecDir[i-1] == 1)) ||
                    ((vecDir[i] == 1) && (vecDir[i-1] == -1)))
            {
                absDifference = abs(nodeDetail.fourADCDetectionPtr->vecPIRDataArray[ADCIndex][i-1] - ADC_ZERO);
                if (absDifference > PEAK_THREHOLD)
                {
                    peakData_t* peakDataPtr = new peakData_t();
                    peakDataPtr->index = i;
                    peakDataPtr->adcValue = nodeDetail.fourADCDetectionPtr->vecPIRDataArray[ADCIndex].at(i-1);
                    peakDataPtr->adcAbsDifference = absDifference;
                    nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].push_back(peakDataPtr);
                }
            }
        }
    }
}

void PeopleDetection::algorithm2PeakFilter(nodeDetail_t &nodeDetail, int ADCIndex)
{
    int i;
    int PeakDataPointersVectorSize = nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].size();
    int vecPeakDataPointersFilterSize = 0;
    int indexDifference;

    nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].clear();
    for (i=0; i<PeakDataPointersVectorSize; i++)
    {
        if (i == 0)
        {
            nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].append(nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at(i));
            vecPeakDataPointersFilterSize++;
        }
        else
        {
            indexDifference = nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at(i)->index - nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at((i-1))->index;
            if (indexDifference > MINUM_PEAK_DIS)
            {
                nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].append(nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at(i));
                vecPeakDataPointersFilterSize++;
            }
            else
            {
                if (nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at(i)->adcAbsDifference > nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].at((vecPeakDataPointersFilterSize-1))->adcAbsDifference)
                {
                    nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].pop_back();
                    nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[ADCIndex].append(nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[ADCIndex].at(i));
                }
            }
        }
    }
}

static bool algorithm2VecDSavePointersCompare(const dSave_t* dSaveStru1,
                                        const dSave_t* dSaveStru2)
{
    return dSaveStru1->peakDataIndexMean < dSaveStru2->peakDataIndexMean;
}

void PeopleDetection::algorithm2DetectDirection(nodeDetail_t &nodeDetail)
{
    int i;
    int vecPeakDataPointersFilterSize;
    float indexMean;
    int vecDSavePointersSize;

    nodeDetail.fourADCDetectionPtr->vecDSavePointers.clear();
    nodeDetail.fourADCDetectionPtr->vecOrder.clear();

    for (i=0; i<MAX_ADC_NUMBER; i++)
    {
        vecPeakDataPointersFilterSize = nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[i].size();
        if (vecPeakDataPointersFilterSize > 1)
        {
            indexMean = (static_cast<float>(nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[i][vecPeakDataPointersFilterSize-2]->index) + static_cast<float>(nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[i][vecPeakDataPointersFilterSize-1]->index)) / 2;
            //printf("PIR%d: %f\n", i, indexMean);
            dSave_t *d_save_ptr = new dSave_t();
            d_save_ptr->adcIndex = i;
            d_save_ptr->peakDataIndexMean = indexMean;
            nodeDetail.fourADCDetectionPtr->vecDSavePointers.append(d_save_ptr);
        }
        else
        {
            //printf("PIR%d_no_data\n", i);
        }
    }

    vecDSavePointersSize = nodeDetail.fourADCDetectionPtr->vecDSavePointers.size();
    if (vecDSavePointersSize > 0)
    {
        sort(nodeDetail.fourADCDetectionPtr->vecDSavePointers.begin(),
            nodeDetail.fourADCDetectionPtr->vecDSavePointers.end(),
            algorithm2VecDSavePointersCompare);
        for (i=0; i<vecDSavePointersSize; i++)
        {
            nodeDetail.fourADCDetectionPtr->vecOrder.append(nodeDetail.fourADCDetectionPtr->vecDSavePointers.at(i)->adcIndex);
        }
    }
    else
    {
        //printf("No valid filter data\n");
        nodeDetail.fourADCDetectionPtr->vecOrder.append(-99);
    }
}

int PeopleDetection::algorithm2GetDirection(nodeDetail_t &nodeDetail)
{
    int i;
    int vecOrderSize = nodeDetail.fourADCDetectionPtr->vecOrder.size();
    int direction = PEOPLE_NEITHER_ENTER_EXIT_DIRECTION_VALUE;
    QVector<int> vecIndex1;
    QVector<int> vecIndex2;

    if (vecOrderSize == 2)
    {
        if (nodeDetail.fourADCDetectionPtr->vecOrder.at(1) == 3)
        {
            direction = PEOPLE_EXIT_DIRECTION_VALUE;
            if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
            {
                if (occupancyNum > DEFAULT_OCCUPANCY_NUM)
                {
                    CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                    "Area(%u) - Node(0x%4.4x): occupancyNum - 1\n",
                                    nodeDetail.fourADCDetectionPtr->id,
                                    nodeDetail.address);
                    occupancyNum--;
                }
            }
        }
    }
    else
    {
        for (i=0; i<vecOrderSize; i++)
        {
            if (nodeDetail.fourADCDetectionPtr->vecOrder.at(i) == 3)
            {
                vecIndex1.append(i);
            }
            else if (nodeDetail.fourADCDetectionPtr->vecOrder.at(i) == 1)
            {
                vecIndex2.append(i);
            }
        }

        if ((vecIndex1.size() > 0) && (vecIndex2.size() >  0))
        {
            if (vecIndex1.at(0) > vecIndex2.at(0))
            {
                direction = PEOPLE_EXIT_DIRECTION_VALUE; // go out
                if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
                {
                    if (occupancyNum > DEFAULT_OCCUPANCY_NUM)
                    {
                        CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                        "Area(%u) - Node(0x%4.4x): occupancyNum - 1\n",
                                        nodeDetail.fourADCDetectionPtr->id,
                                        nodeDetail.address);
                        occupancyNum--;
                    }
                }
            }
            else
            {
                direction = PEOPLE_ENTER_DIRECTION_VALUE; // go in
                if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
                {
                    CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                    "Area(%u) - Node(0x%4.4x): occupancyNum + 1\n",
                                    nodeDetail.fourADCDetectionPtr->id,
                                    nodeDetail.address);
                    occupancyNum++;
                }
            }
        }
    }

#if DATA_SAVING_WHILE_COMPUTING
    //NECESSARY_LOG("Do results logging\n");
    if (nodeDetail.algorithmResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
    {
        nodeDetail.algorithmResultsLoggingFile.write(
                    QString().sprintf("%s,%d,%d\n",
                    QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss").toLatin1().data(),
                    direction,
                    occupancyNum).toLatin1().data());
        nodeDetail.algorithmResultsLoggingFile.close();
    }
#endif // DATA_SAVING_WHILE_COMPUTING
    return direction;
}

void PeopleDetection::algorithm2GetDirectionResult(nodeDetail_t &nodeDetail)
{
    int i, j;

    for (i=0; i<MAX_ADC_NUMBER; i++)
    {
        algorithm2DetectPeak(nodeDetail, i);
        algorithm2PeakFilter(nodeDetail, i);
    }

    algorithm2DetectDirection(nodeDetail);

    NECESSARY_LOG("direction is %d\n", algorithm2GetDirection(nodeDetail));

    for (i=0; i<MAX_ADC_NUMBER; i++)
    {
        for (j=0; j<nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[i].size(); j++)
        {
            delete nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[i][j];
        }

        nodeDetail.fourADCDetectionPtr->vecPeakDataPointers[i].clear();
        nodeDetail.fourADCDetectionPtr->vecPeakDataPointersFilter[i].clear();
        nodeDetail.fourADCDetectionPtr->vecPIRDataArray[i].clear();
    }

    for (i=0; i<nodeDetail.fourADCDetectionPtr->vecDSavePointers.size(); i++)
    {
        delete nodeDetail.fourADCDetectionPtr->vecDSavePointers[i];
    }

    nodeDetail.fourADCDetectionPtr->vecOrder.clear();
    nodeDetail.fourADCDetectionPtr->vecDSavePointers.clear();
}

void PeopleDetection::algorithm2ChillingTimerTimeout(void)
{
    uint8_t i;
    QTimer *chillTimer;

    chillTimer = dynamic_cast<QTimer *>(sender());

    if (chillTimer)
    {
        for (i=0; i<MAX_NODE_NUMBER; i++)
        {
            if ((nodeDetails[i].type == NODE_TYPE_FOUR_ADC) &&
                (nodeDetails[i].fourADCDetectionPtr->chillTimer == chillTimer))
            {
                nodeDetails[i].fourADCDetectionPtr->ifInChillingProcess = false;
                break;
            }
        }
    }
}

void PeopleDetection::algorithm2ForcedCalculationTimerTimeout(void)
{
    uint8_t i;
    QTimer *forcedCalculationTimer;

    forcedCalculationTimer = dynamic_cast<QTimer *>(sender());

    if (forcedCalculationTimer)
    {
        for (i=0; i<MAX_NODE_NUMBER; i++)
        {
            if ((nodeDetails[i].type == NODE_TYPE_FOUR_ADC) &&
                (nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer == forcedCalculationTimer))
            {
                nodeDetails[i].fourADCDetectionPtr->ifForcedToCalculate = true;
                break;
            }
        }
    }
}
#endif // ENABLE_ALGORITHM2_CALCULATION

#if ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
void PeopleDetection::updatingOccupancyResultTimeout(void)
{
    bool ifOccupancyed = false;
    uint8_t i;
    bool nodeResults[TOTAL_CONSIDER_ELEMENTS_NUMBER] = {false, false, false, false, false, false, false, false};
    bool stateMachineResult = false;
    bool internalOccupancyed = false;
    QVector<int>::iterator occupancyStateSavedIter;
    QVector<uint32_t>::iterator hubIter;
    char pnnlLoggingBuffer[50];
    int sprintfIndex = 0;

    memset(pnnlLoggingBuffer, '\0', sizeof(char) * 50);

    // Check hub
    for (hubIter = vecClientGPIOStatus.begin();
         hubIter != vecClientGPIOStatus.end();
         hubIter++)
    {
        if (*hubIter == 1)
        {
            nodeResults[HUB_INDEX_FROM_RESULTS] = true;
            break;
        }
    }

    if ((mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED_INDEX] == false) &&
        (mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN_INDEX] == false))
    {
        if (mainWindowPtr->hubRawDataLoggingFile.open(QIODevice::Append | QIODevice::Text))
        {
            mainWindowPtr->hubRawDataLoggingFile.write(
                        QString().sprintf("%s,%d\n",
                        QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                        (nodeResults[HUB_INDEX_FROM_RESULTS] == true)?(1):(0)).toLatin1().data());
            mainWindowPtr->hubRawDataLoggingFile.close();
        }
    }

    sprintfIndex += sprintf(pnnlLoggingBuffer, "%s,",
                           QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss").toLatin1().data());

    // Check each nodes
    for (i=0; i<MAX_NODE_NUMBER; i++)
    {
        if (nodeDetails[i].address != DEFAULT_NODE_ADDRESS)
        {
            switch(nodeDetails[i].type)
            {
                case NODE_TYPE_TWO_ADC:
#if ENABLE_ALGORITHM1_CALCULATION
                    if (nodeDetails[i].twoADCDetectionPtr != nullptr)
                    {
                        if (nodeDetails[i].twoADCDetectionPtr->ifInitialed)
                        {
                            for (occupancyStateSavedIter = nodeDetails[i].twoADCDetectionPtr->occupancyStateSaved.begin();
                                 occupancyStateSavedIter != nodeDetails[i].twoADCDetectionPtr->occupancyStateSaved.end();
                                 occupancyStateSavedIter++)
                            {
                                if (*occupancyStateSavedIter == 1)
                                {
                                    nodeResults[nodeDetails[i].twoADCDetectionPtr->id - 1] = true;
                                    // update the node's detection result on the screen
                                    mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id - 1].detectionResultLabel->setText(NODE_DETECTION_RESULT_POSITIVE);
                                    break;
                                }
                            }

                            if (occupancyStateSavedIter == nodeDetails[i].twoADCDetectionPtr->occupancyStateSaved.end())
                            {
                                // negative
                                // update the node's detection result on the screen
                                mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id - 1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NEGATIVE);
                            }

                            CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                            "Area(%d) -> %d\n",
                                            nodeDetails[i].twoADCDetectionPtr->id,
                                            (nodeResults[nodeDetails[i].twoADCDetectionPtr->id - 1] == true)?(1):(0));

                            if (nodeDetails[i].twoADCDetectionPtr->vecPeriodTemperature.size())
                            {
                                sprintfIndex += sprintf(pnnlLoggingBuffer + sprintfIndex, "%2.2f,", nodeDetails[i].twoADCDetectionPtr->vecPeriodTemperature[0]);
                            }
                        }
                    }
#endif // ENABLE_ALGORITHM1_CALCULATION
                    break;
                case NODE_TYPE_FOUR_ADC:
#if ENABLE_ALGORITHM2_CALCULATION
                    if (nodeDetails[i].fourADCDetectionPtr != nullptr)
                    {
                        if (nodeDetails[i].fourADCDetectionPtr->ifMotionInOnePeriod)
                        {
                            nodeResults[nodeDetails[i].fourADCDetectionPtr->id - 1] = true;
                            nodeDetails[i].fourADCDetectionPtr->ifMotionInOnePeriod = false;
                            // update the node's detection result on the screen
                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id - 1].detectionResultLabel->setText(NODE_DETECTION_RESULT_POSITIVE);
                        }
                        else
                        {
                            // update the node's detection result on the screen
                            mainWindowPtr->InteractionNodes[nodeDetails[i].fourADCDetectionPtr->id - 1].detectionResultLabel->setText(NODE_DETECTION_RESULT_NEGATIVE);
                        }

                        CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                        "Area(%d) -> %d\n",
                                        nodeDetails[i].fourADCDetectionPtr->id,
                                        (nodeResults[nodeDetails[i].fourADCDetectionPtr->id - 1] == true)?(1):(0));
                    }
#endif // ENABLE_ALGORITHM2_CALCULATION
                    break;
                default:
                    break;
            }
        }
    }

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                    "Hub     -> %d\n", nodeResults[HUB_INDEX_FROM_RESULTS]);

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "occupancyNum is %d\n",
                    occupancyNum);

    if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
    {
    #if DO_STATE_MACHINE_TRANSITION
        // Do the state machine transition
        switch(detectionState)
        {
            case DETECTION_STATE_INIT:
                if (nodeResults[0] ||   // node 1
                    nodeResults[1] ||   // node 2
                    nodeResults[2])     // node 3
                {
                    CONDITIONAL_LOG(true, "init -> S1\n");
                    detectionState = DETECTION_STATE_S1;
                    stateMachineResult = true;
                }
                break;
            case DETECTION_STATE_S1:
                if (nodeResults[3] ||   //node 4
                    nodeResults[4] ||   //node 5
                    nodeResults[5] ||   //node 6
                    nodeResults[6] ||   //node 7
                    nodeResults[HUB_INDEX_FROM_RESULTS])     //hub
                {
                    // goes inside
                    CONDITIONAL_LOG(true, "S1 -> S3\n");
                    detectionState = DETECTION_STATE_S3;
                    stateMachineResult = true;
                    internalOccupancyed = true;
                }
                else if (nodeResults[0] ||
                         nodeResults[1] ||
                         nodeResults[2])
                {
                    // stay S1
                    CONDITIONAL_LOG(true, "S1 -> S1\n");
                    stateMachineResult = true;
                }
                else
                {
                    // goes outside
                    CONDITIONAL_LOG(true, "S1 -> S2\n");
                    detectionState = DETECTION_STATE_S2;
                }
                break;
            case DETECTION_STATE_S2:
                if (nodeResults[0] ||
                    nodeResults[1] ||
                    nodeResults[2])
                {
                    CONDITIONAL_LOG(true, "S2 -> S1\n");
                    detectionState = DETECTION_STATE_S1;
                    stateMachineResult = true;
                }
                // state backward
                else if((occupancyNum == 0) && (
                        nodeResults[3] ||
                        nodeResults[4] ||
                        nodeResults[5] ||
                        nodeResults[6] ||
                        nodeResults[HUB_INDEX_FROM_RESULTS]))
                {
                    CONDITIONAL_LOG(true, "S2 -> S3\n");
                    detectionState = DETECTION_STATE_S3;
                    stateMachineResult = true;
                    internalOccupancyed = true;
                }
                break;
            case DETECTION_STATE_S3:
                if ((nodeResults[3] ||
                     nodeResults[4] ||
                     nodeResults[5] ||
                     nodeResults[6] ||
                     nodeResults[HUB_INDEX_FROM_RESULTS]) && (nodeResults[0] ||
                                                              nodeResults[1] ||
                                                              nodeResults[2]))
                {
                    CONDITIONAL_LOG(true, "S3 -> S1\n");
                    detectionState = DETECTION_STATE_S1;
                }
                else
                {
                    if (nodeResults[3] ||
                        nodeResults[4] ||
                        nodeResults[5] ||
                        nodeResults[6] ||
                        nodeResults[HUB_INDEX_FROM_RESULTS])
                    {
                        // Stay S3
                        CONDITIONAL_LOG(true, "S3 -> S3\n");
                    }
                    else if (nodeResults[0] ||
                        nodeResults[1] ||
                        nodeResults[2])
                    {
                        // Stay S3
                        CONDITIONAL_LOG(true, "S3 -> S3: doors positive\n");
                    }
                    else
                    {
                        // enter unknown area
                        CONDITIONAL_LOG(true, "S3 -> S3: unknown area\n");
                    }
                    internalOccupancyed = true;
                }
                stateMachineResult = true;
                break;
        }
    #endif // DO_STATE_MACHINE_TRANSITION

        // get the final result
        if (occupancyNum > DEFAULT_OCCUPANCY_NUM)
        {
            ifOccupancyed = true;
        }
        else if (stateMachineResult)
        {
            ifOccupancyed = true;
            if (internalOccupancyed)
            {
                occupancyNum = DEFAULT_OCCUPANCY_EXIST_NUM;
            }
        }
    }

#if ENABLE_FILE_OUTPUTS_TESTING
    if (mainWindowPtr->pnnlOccupancyResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
    {
        sprintfIndex += sprintf(pnnlLoggingBuffer + sprintfIndex, "%d\n", (ifOccupancyed == true)?(1):(0));

        mainWindowPtr->pnnlOccupancyResultsLoggingFile.write(pnnlLoggingBuffer);
        mainWindowPtr->pnnlOccupancyResultsLoggingFile.close();
    }

    if (mainWindowPtr->occupancyResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
    {
        mainWindowPtr->occupancyResultsLoggingFile.write(
                    QString().sprintf("%s,%d\n",
                    QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                    (ifOccupancyed == true)?(1):(0)).toLatin1().data());
        mainWindowPtr->occupancyResultsLoggingFile.close();
    }
#endif // ENABLE_FILE_OUTPUTS_TESTING

    vecClientGPIOStatus.clear();

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                    "initOKNodesNum is %u and registeredNodesNum is %u\n",
                    initOKNodesNum,
                    registeredNodesNum);

    if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
    {
        mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NODES_NOT_COMPLETE_INDEX] = false;

        mainWindowPtr->systemIssueDetectionResultLabel->setText(GENERAL_ISSUE_DETECTION_RESULT_NONE);
        if (ifOccupancyed == true)
        {
            mainWindowPtr->occupancyDetectionResultLabel->setText(OCCUPANCY_DETECTION_RESULT_POSITIVE_CONTENT);
            #if defined(Q_OS_LINUX)
            #ifdef QT_ON_RPI
            digitalWrite(1, HIGH);
            #endif // QT_ON_RPI
            #endif
        }
        else
        {
            mainWindowPtr->occupancyDetectionResultLabel->setText(OCCUPANCY_DETECTION_RESULT_NEGATIVE_CONTENT);
            #if defined(Q_OS_LINUX)
            #ifdef QT_ON_RPI
            digitalWrite(1, LOW);
            #endif // QT_ON_RPI
            #endif
        }
#if !ENABLE_FILE_OUTPUTS_TESTING
        if (mainWindowPtr->occupancyResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
        {
            mainWindowPtr->occupancyResultsLoggingFile.write(
                        QString().sprintf("%s,%d\n",
                        QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                        (ifOccupancyed == true)?(1):(0)).toLatin1().data());
            mainWindowPtr->occupancyResultsLoggingFile.close();
        }

        if (mainWindowPtr->pnnlOccupancyResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
        {
            sprintfIndex += sprintf(pnnlLoggingBuffer + sprintfIndex, "%d\n", (ifOccupancyed == true)?(1):(0));

            mainWindowPtr->pnnlOccupancyResultsLoggingFile.write(pnnlLoggingBuffer);
            mainWindowPtr->pnnlOccupancyResultsLoggingFile.close();
        }
#endif // ENABLE_FILE_OUTPUTS_TESTING
    }
    else
    {
        mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NODES_NOT_COMPLETE_INDEX] = true;
    }

#if ADD_SYSTEM_ISSUE_CHECKING
    for (i=0; i<GENERAL_ISSUE_NUMBER; i++)
    {
        if (mainWindowPtr->ifSystemHasIssuesArray[i] == true)
        {
            mainWindowPtr->systemIssueDetectionResultLabel->setText(mainWindowPtr->systemHasIssuesStringsArray[i]);

            mainWindowPtr->occupancyDetectionResultLabel->setText(OCCUPANCY_DETECTION_RESULT_NULL_CONTENT);
            // 默认置0
            #if defined(Q_OS_LINUX)
            #ifdef QT_ON_RPI
            digitalWrite(1, LOW);
            #endif // QT_ON_RPI
            #endif
            break;
        }
    }
#else
    if (ifOccupancyed == true)
    {
        mainWindowPtr->occupancyDetectionResultLabel->setText(OCCUPANCY_DETECTION_RESULT_POSITIVE_CONTENT);
        #if defined(Q_OS_LINUX)
        #ifdef QT_ON_RPI
        digitalWrite(1, HIGH);
        #endif // QT_ON_RPI
        #endif
    }
    else
    {
        mainWindowPtr->occupancyDetectionResultLabel->setText(OCCUPANCY_DETECTION_RESULT_NEGATIVE_CONTENT);
        #if defined(Q_OS_LINUX)
        #ifdef QT_ON_RPI
        digitalWrite(1, LOW);
        #endif // QT_ON_RPI
        #endif
    }
    if (mainWindowPtr->occupancyResultsLoggingFile.open(QIODevice::Append | QIODevice::Text))
    {
        mainWindowPtr->occupancyResultsLoggingFile.write(
                    QString().sprintf("%s,%d\n",
                    QTime::currentTime().toString("hh:mm:ss").toLatin1().data(),
                    (ifOccupancyed == true)?(1):(0)).toLatin1().data());
        mainWindowPtr->occupancyResultsLoggingFile.close();
    }
#endif // ADD_SYSTEM_ISSUE_CHECKING
}
#endif // ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION
#endif // ENABLE_ALGORITHM_CALCULATION

// private slots
void PeopleDetection::serialMonitorTimerTimeout(void)
{
    mainWindowPtr->serialConnectionStatusLabel->setText(SERIAL_CONNECTION_STATUS_BAD_CONTENT);
    mainWindowPtr->ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN_INDEX] = true;
}

void PeopleDetection::nodeDetectionInit(void)
{
    uint8_t i, j;
    QPushButton *pushButton;

    pushButton = dynamic_cast<QPushButton *>(sender());

    if (pushButton)
    {
        for (i=0; i<MAX_NODE_NUMBER; i++)
        {
            if (pushButton == mainWindowPtr->InteractionNodes[i].resetPushButton)
            {
                for (j=0; j<MAX_NODE_NUMBER; j++)
                {
                    // Do the init and clear all the existing variables
                    if (mainWindowPtr->InteractionNodes[i].address == nodeDetails[j].address)
                    {
                        if ((nodeDetails[j].type == NODE_TYPE_TWO_ADC) &&
                            (nodeDetails[j].twoADCDetectionPtr != nullptr))
                        {
                            CONDITIONAL_LOG(ifDynamicResultsLoggingOn,
                                            "Node(0x%4.4x) is re-init\n",
                                            nodeDetails[j].address);
#if ENABLE_ALGORITHM1_CALCULATION
                            if (nodeDetails[j].twoADCDetectionPtr->ifInitialed)
                            {
                                initOKNodesNum--;
                            }
#if ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[j].twoADCDetectionPtr->ifInitialed = false;
#else // !ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[j].twoADCDetectionPtr->ifInitialed = true;
#endif // ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[j].twoADCDetectionPtr->initTimesCounter = 0;
                            nodeDetails[j].twoADCDetectionPtr->vecInitFrameAnalysis.clear();
                            nodeDetails[j].twoADCDetectionPtr->vecFrameAnalysisResults.clear();
                            nodeDetails[j].twoADCDetectionPtr->vecFrameAnalysisResultsA.clear();
                            nodeDetails[j].twoADCDetectionPtr->vecFrameAnalysisResultsB.clear();
                            nodeDetails[j].twoADCDetectionPtr->occupancyStateSaved.clear();
                            nodeDetails[j].twoADCDetectionPtr->occupancyState = 0;
                            nodeDetails[j].twoADCDetectionPtr->occupancySwitch = 0;
                            nodeDetails[j].twoADCDetectionPtr->motionLength = 0;
                            nodeDetails[j].twoADCDetectionPtr->AState = 0;
#endif // ENABLE_ALGORITHM1_CALCULATION

                            mainWindowPtr->InteractionNodes[nodeDetails[j].twoADCDetectionPtr->id - 1].resetStatusLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                            mainWindowPtr->InteractionNodes[nodeDetails[j].twoADCDetectionPtr->id - 1].detectionResultLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                        }

                        break;
                    }
                }
                break;
            }
        }
    }
}

void PeopleDetection::overallResetTimerTimeout(void)
{
    uint8_t i;

    CONDITIONAL_LOG(ifDynamicResultsLoggingOn, "overallResetTimerTimeout\n");
    if (initOKNodesNum == MAX_NODE_NUMBER && registeredNodesNum == MAX_NODE_NUMBER)
    {
        for (i=0; i<MAX_NODE_NUMBER; i++)
        {
            if ((nodeDetails[i].address != DEFAULT_NODE_ADDRESS) &&
                (nodeDetails[i].type != NODE_TYPE_NULL))
            {
                switch (nodeDetails[i].type)
                {
                    case NODE_TYPE_TWO_ADC:
    #if ENABLE_ALGORITHM1_CALCULATION
                        if (nodeDetails[i].twoADCDetectionPtr != nullptr)
                        {
                            if (nodeDetails[i].twoADCDetectionPtr->ifInitialed)
                            {
                                initOKNodesNum--;
                            }
        #if ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[i].twoADCDetectionPtr->ifInitialed = false;
        #else // !ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[i].twoADCDetectionPtr->ifInitialed = true;
        #endif // ENABLE_ALGORITHM1_FORCED_INITIALIZATION
                            nodeDetails[i].twoADCDetectionPtr->initTimesCounter = 0;
                            //nodeDetails[i].twoADCDetectionPtr->linesContent.clear();
                            nodeDetails[i].twoADCDetectionPtr->vecInitFrameAnalysis.clear();
                            nodeDetails[i].twoADCDetectionPtr->vecFrameAnalysisResults.clear();
                            nodeDetails[i].twoADCDetectionPtr->vecFrameAnalysisResultsA.clear();
                            nodeDetails[i].twoADCDetectionPtr->vecFrameAnalysisResultsB.clear();
                            nodeDetails[i].twoADCDetectionPtr->occupancyStateSaved.clear();
                            nodeDetails[i].twoADCDetectionPtr->occupancyState = 0;
                            nodeDetails[i].twoADCDetectionPtr->occupancySwitch = 0;
                            nodeDetails[i].twoADCDetectionPtr->motionLength = 0;
                            nodeDetails[i].twoADCDetectionPtr->AState = 0;

                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id - 1].resetStatusLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                            mainWindowPtr->InteractionNodes[nodeDetails[i].twoADCDetectionPtr->id - 1].detectionResultLabel->setText(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT);
                        }
    #endif // ENABLE_ALGORITHM1_CALCULATION
                        break;
                    case NODE_TYPE_FOUR_ADC:
                        if (nodeDetails[i].fourADCDetectionPtr != nullptr)
                        {
                            nodeDetails[i].fourADCDetectionPtr->ifMotionInOnePeriod = false;
                            nodeDetails[i].fourADCDetectionPtr->ifCalculationInProgress = false;
                            nodeDetails[i].fourADCDetectionPtr->ifForcedToCalculate = false;
                            nodeDetails[i].fourADCDetectionPtr->ifInChillingProcess = false;
                            nodeDetails[i].fourADCDetectionPtr->ifGPIOHigh = false;

                            nodeDetails[i].fourADCDetectionPtr->chillTimer->stop();
                            nodeDetails[i].fourADCDetectionPtr->forcedCalculationTimer->stop();
                        }

                        break;
                    default:
                        break;
                }
            }
        }

        occupancyNum = DEFAULT_OCCUPANCY_NUM;
    }
}

void PeopleDetection::dateAdvancedHandle(QString &currentDateString)
{
    NECESSARY_LOG("Received updated date: %s\n", currentDateString.toLatin1().data());
    // update all the nodes' filename prefixes
#if DATA_SAVING_WHILE_COMPUTING
    // Create the files
    uint8_t i;

    // Create the new occupancy logging file
    mainWindowPtr->occupancyResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s%s%s",
                                                                                              currentDateString.toLatin1().data(),
                                                                                              SLEEPIR_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING,
                                                                                              DEFAULT_LOGGING_FILE_TYPE_STRING)));
    if (mainWindowPtr->occupancyResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
    {
        mainWindowPtr->occupancyResultsLoggingFile.close();
    }

    // Create the new hub files
    mainWindowPtr->hubRawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s%s%s",
                                                          currentDateString.toLatin1().data(),
                                                          SLEEPIR_HUB_RAW_DATA_LOGGING_FILE_NAME_STRING,
                                                          DEFAULT_LOGGING_FILE_TYPE_STRING)));
    if (mainWindowPtr->hubRawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
    {
        mainWindowPtr->hubRawDataLoggingFile.close();
    }

    for (i=0; i<MAX_NODE_NUMBER; i++)
    {
        if ((nodeDetails[i].type != NODE_TYPE_NULL) &&
            (nodeDetails[i].address != DEFAULT_NODE_ADDRESS))
        {
            nodeDetails[i].rawDataLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                           currentDateString.toLatin1().data(),
                                                           (nodeDetails[i].type == NODE_TYPE_TWO_ADC)?(TWO_ADC_FILE_NAME_MIDDLE_PART):(FOUR_ADC_FILE_NAME_MIDDLE_PART),
                                                           nodeDetails[i].address,
                                                           NODE_RAW_DATA_LOGGING_FILENAME_STRING,
                                                           DEFAULT_LOGGING_FILE_TYPE_STRING)));
            if (nodeDetails[i].rawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
            {
                nodeDetails[i].rawDataLoggingFile.close();
            }

            nodeDetails[i].algorithmResultsLoggingFile.setFileName(mainWindowPtr->sleepirLoggingFilesDir.filePath(QString().sprintf("%s_%s_0x%4.4x%s%s",
                                                                    currentDateString.toLatin1().data(),
                                                                    (nodeDetails[i].type == NODE_TYPE_TWO_ADC)?(TWO_ADC_FILE_NAME_MIDDLE_PART):(FOUR_ADC_FILE_NAME_MIDDLE_PART),
                                                                    nodeDetails[i].address,
                                                                    NODE_RESULTS_LOGGING_FILENAME_STRING,
                                                                    DEFAULT_LOGGING_FILE_TYPE_STRING)));
            if (nodeDetails[i].algorithmResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
            {
                nodeDetails[i].algorithmResultsLoggingFile.close();
            }
        }
    }
#endif // DATA_SAVING_WHILE_COMPUTING
}
