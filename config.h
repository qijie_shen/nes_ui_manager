#ifndef CONFIG_H
#define CONFIG_H

#include <stdint.h>

// Mainwindow defines
#define CONFIG_MAINWINDOW_FULL_SCREEN                           (0)
#if !CONFIG_MAINWINDOW_FULL_SCREEN
#define CONFIG_MAINWINDOW_SCREEN_WIDTH                          (800)
#define CONFIG_MAINWINDOW_SCREEN_HEIGHT                         (600)
#endif // !CONFIG_MAINWINDOW_FULL_SCREEN

// general defines
#define MAX_NODE_NUMBER                                         (7)
#define TOTAL_CONSIDER_ELEMENTS_NUMBER                          (MAX_NODE_NUMBER + 1)

#define HUB_INDEX_FROM_RESULTS                                  (MAX_NODE_NUMBER)

#define ENABLE_DEVELOPER_MODE                                   (0)
#if !ENABLE_DEVELOPER_MODE
#define DEFAULT_FLOAT_PRECISION                                 (2)
#endif // ENABLE_DEVELOPER_MODE

#define DATA_SAVING_WHILE_COMPUTING                             (1)
#define ENABLE_ALGORITHM_FINAL_RESULT_INTEGRATION               (1)

#define DEFAULT_NODE_ADDRESS                                    (0)

#define ENABLE_ALGORITHM_CALCULATION                            (1)
#if ENABLE_ALGORITHM_CALCULATION

#define ENABLE_ALGORITHM1_CALCULATION                           (1)
#if ENABLE_ALGORITHM1_CALCULATION
#define ENABLE_ALGORITHM1_FORCED_INITIALIZATION                 (1)
#endif // ENABLE_ALGORITHM1_CALCULATION

#define ENABLE_ALGORITHM2_CALCULATION                           (1)

#endif // ENABLE_ALGORITHM_CALCULATION

#define DO_STATE_MACHINE_TRANSITION                             (1)

#define ADD_SYSTEM_ISSUE_CHECKING                               (1)

#define ENABLE_HUMIDITY_PARSING                                 (0)

#define ENABLE_DEBUGGING_PRINTS                                 (0)

#define ENABLE_FILE_OUTPUTS_TESTING                             (0)

#endif // CONFIG_H
