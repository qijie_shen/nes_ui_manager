#include "mainwindow.h"
#include <QApplication>

#include "config.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
#if CONFIG_MAINWINDOW_FULL_SCREEN
    w.showMaximized();
#else // !CONFIG_MAINWINDOW_FULL_SCREEN
    w.show();
#endif // CONFIG_MAINWINDOW_FULL_SCREEN

    return a.exec();
}
