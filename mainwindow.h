#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>
#include <QtWidgets/QLabel>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QTimeEdit>

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QMessageBox>
#include <QFile>
#include <QDir>

#include "config.h"

#define INTERACTION_GROUP_BOX_NAME                              "Interaction"

//
#define DATE_TIME_GROUP_BOX_NAME                                "Date-time display"
#define CLOCK_DISPLAY_LABEL_STRING                              "System time: "
#define CLOCK_DISPLAY_REFRESH_PERIOD                            (1000)    // 1000ms -> 1s
#define SET_CLOCK_LABEL_STRING                                  "Set system time: "
#define GET_CURRENT_TIME_BUTTON_NAME                            "Get current time"
#define SET_NEW_TIME_BUTTON_NAME                                "Set new time"

//
#define MISC_STATUS_INDICATION_GROUP_BOX_NAME                   "Misc status indication"
// Serial connection status
#define SERIAL_CONNECTION_STATUS_DEC_LABEL_CONTENT              "Serial connection status: "
#define SERIAL_CONNECTION_STATUS_GOOD_CONTENT                   "good"
#define SERIAL_CONNECTION_STATUS_BAD_CONTENT                    "bad"
#define SERIAL_CONNECTION_STATUS_NULL_CONTENT                   "NULL"

// Occupancy detection result
#define OCCUPANCY_DETECTION_RESULT_DEC_LABEL_CONTENT            "Occupancy detection result: "
#define OCCUPANCY_DETECTION_RESULT_POSITIVE_CONTENT             "1"
#define OCCUPANCY_DETECTION_RESULT_NEGATIVE_CONTENT             "0"
#define OCCUPANCY_DETECTION_RESULT_NULL_CONTENT                 "NULL"

// Issue detection result
#define GENERAL_ISSUE_DETECTION_RESULT_DEC_LABEL_CONTENT        "System running issue: "
#define GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED            "Connection button not pressed"
#define GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN              "Hub is down"
#define GENERAL_ISSUE_DETECTION_RESULT_NODE_ID_CONFLICT         "Node id conflict"
#define GENERAL_ISSUE_DETECTION_RESULT_NODEs_ARE_NOT_COMPLETE   "Nodes not complete"
#define GENERAL_ISSUE_DETECTION_RESULT_NONE                     "None"

#define GENERAL_ISSUE_NUMBER                                    4
#define GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED_INDEX      0
#define GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN_INDEX        1
#define GENERAL_ISSUE_DETECTION_RESULT_NODE_ID_CONFLICT_INDEX   2
#define GENERAL_ISSUE_DETECTION_RESULT_NODES_NOT_COMPLETE_INDEX 3

#define OCCUPANCY_DETECTION_RESULT_POSITIVE_CONTENT             "1"
#define OCCUPANCY_DETECTION_RESULT_NEGATIVE_CONTENT             "0"
#define OCCUPANCY_DETECTION_RESULT_NULL_CONTENT                 "NULL"

// Connection status
#define CONNECTION_STATUS_GROUP_BOX_NAME                        "Network status"
#define CONNECTION_STATUS_AREA_ID_DCE_LABEL_CONTENT             "AreaID: "

#define CONNECTION_STATUS_NODE_ADDRESS_DEC_LABEL_CONTENT        "Node Address: "

#define CONNECTION_STATUS_DEC_LABEL_CONTENT                     "Connection status: "
#define CONNECTION_STATUS_CONNECTED_LABEL_CONTENT               "Connected"
#define CONNECTION_STATUS_DISCONNECTED_LABEL_CONTENT            "Disconnected"

#define CONNECTION_STATUS_RESET_BUTTON_LABEL_CONTENT            "Node init"
#define CONNECTION_STATUS_INIT_STATUS_DEC_LABEL_CONTENT         "Init status: "
#define CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT       "Null"
#define CONNECTION_STATUS_INIT_STATUS_SUCCESS_LABEL_CONTENT     "Succeeded"
#define CONNECTION_STATUS_INIT_STATUS_FAILED_LABEL_CONTENT      "Failed"

#define CONNECTION_STATUS_DETECTION_RESULT_DEC_LABEL_CONTENT    "Detection Result: "

#define NODE_DETECTION_RESULT_NEGATIVE                          "0"
#define NODE_DETECTION_RESULT_POSITIVE                          "1"
#define NODE_DETECTION_RESULT_NULL                              "NULL"

#define SETTINGS_GROUPBOX_LAYOUT_STRETCH_FACTOR                 (1)
#define INTERACTION_GROUPBOX_LAYOUT_STRETCH_FACTOR              (6)

#define DATE_TIME_UPDATE_WAIT_SECS_TO_POPUP_MBOX_COUNTER        (10)

#define TIME_CALIBRATION_IS_DONE_CONTENT                        "Time is set!!"

#define PNNL_DIR_NAME_STRING                                    "PNNL"
#define PNNL_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING         "pnnl_occupancy_results"

#define SLEEPIR_DIR_NAME_STRING                                 "SLEEPIR"
#define SLEEPIR_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING      "_occupancy_results"
#define SLEEPIR_HUB_RAW_DATA_LOGGING_FILE_NAME_STRING           "_hub_raw_data"

namespace Ui {
    class MainWindow;
}

typedef struct
{
    uint16_t address;
    QLabel *addressLabel;
    QLabel *connectionStatusLabel;
    QPushButton *resetPushButton;
    QLabel *resetStatusLabel;
    QLabel *detectionResultLabel;
}InteractionNode_t;

class PeopleDetection;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    struct Settings
    {
        QString name;
        qint32 baudRate;
        QString stringBaudRate;
        QSerialPort::DataBits dataBits;
        QString stringDataBits;
        QSerialPort::Parity parity;
        QString stringParity;
        QSerialPort::StopBits stopBits;
        QString stringStopBits;
        QSerialPort::FlowControl flowControl;
        QString stringFlowControl;
#if 0
        QString hostIP;
        QString port;
#endif
#if ENABLE_DEVELOPER_MODE
        double plotYAxisMinThresholdValue;
        double plotYAxisMaxThresholdValue;

        int floatPrecision;
#endif // ENABLE_DEVELOPER_MODE
    };

signals:
    void updateDatePartOfFileNames(QString &currentDateString);

private slots:
    // interaction
    void systemTimeDisplayUpdate(void);
    void getCurrentTimeToEdit(void);
    void setSystemTimeFromDateTimeEdit(void);
    // settings
    void apply();
    void showPortInfo(int idx);
    void checkCustomBaudratePolicy(int idx);
    void checkCustomDevicePathPolicy(int idx);
    // serial
    void openSerialPort();
    void closeSerialPort();
    void readData();
    void handleError(QSerialPort::SerialPortError error);

public:
    QLabel *serialConnectionStatusLabel;
    QLabel *occupancyDetectionResultLabel;
    QLabel *systemIssueDetectionResultLabel;
    InteractionNode_t InteractionNodes[MAX_NODE_NUMBER];
    QString currentDateString;

    QDir pnnlLoggingFilesDir;
    QFile pnnlOccupancyResultsLoggingFile;

    QDir sleepirLoggingFilesDir;
    QFile occupancyResultsLoggingFile;
    QFile hubRawDataLoggingFile;

    bool ifSystemHasIssuesArray[GENERAL_ISSUE_NUMBER];

    const char* systemHasIssuesStringsArray[GENERAL_ISSUE_NUMBER] =
    {
        GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED,
        GENERAL_ISSUE_DETECTION_RESULT_HUB_IS_DOWN,
        GENERAL_ISSUE_DETECTION_RESULT_NODE_ID_CONFLICT,
        GENERAL_ISSUE_DETECTION_RESULT_NODEs_ARE_NOT_COMPLETE,
    };

    bool ifSerialConnected = false;

private:
    // ui functions
    void setupUi(void);
    void retranslateUi(void);

    // interaction functions
    void setupSettingsUi(void);
    void retranslateSettingsUi(void);

    // settings functions
    void setupInteractionUi(void);
    void retranslateInteractionUi(void);

    void fillPortsParameters();
    void fillPortsInfo();
    void updateSettings();

    void createDataTimeGroupBox(void);
    // void createMaterialSelectionGroupBox(void);
    void createMiscStatusIndicationGroupBox(void);
    void createNetworkStatusGroupBox(void);

    // serial
    void showStatusMessage(const QString &message);

    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QWidget *centralWidget;
    QStatusBar *statusBar;

    QLabel *mStatusLabel = nullptr;
    QSerialPort *mSerial = nullptr;

    QHBoxLayout *centralHorizontalLayout;
    QGroupBox *centralGroupBox;

    // Interaction widgets
    QGroupBox *interactionGroupBox;

    // Data-time widgets
    QGroupBox *dateTimeGroupBox;
    QTimer *timeRefreshTimer;
    QLabel *currentDateTimeLabel;

    // Message box interaction
    bool ifDateTimeCalibrated = false;
    uint8_t ifDateTimeCalibratedMboxPopupCounter = 0;

    QDateTimeEdit *dateTimeEdit;
    QPushButton *getCurrentTimeForEditPushButton;
    QPushButton *setNewSystemTimePushButton;

    // Misc status indication widgets
    QGroupBox *miscStatusIndicationGroupBox;

    // Connection status widgets
    QGroupBox *nodesConnectionGroupBox;

    // Setup widgets
    QGroupBox *settingsGroupBox;
    QVBoxLayout *settingsVerticalLayout;

    QHBoxLayout *applyHorizontalLayout;
    QPushButton *applyButton;
    QPushButton *connectButton;
    QPushButton *disconnectButton;

    // Serial selection
    QGroupBox *serialSelectGroupBox;
    QGridLayout *serialSelectGridLayout;
    QComboBox *serialPortInfoListBox;
    QLabel *descriptionLabel;
    QLabel *manufacturerLabel;
    QLabel *serialNumberLabel;
    QLabel *locationLabel;
    QLabel *vidLabel;
    QLabel *pidLabel;

    QGroupBox *parametersBox;
    QGridLayout *parametersGridLayout;

    // TCP server
#if 0
    QLabel *hostLabel;
    QLabel *portLabel;
    QLineEdit *hostText;
    QLineEdit *portText;
#endif

    // Serial parameters
    QLabel *baudRateLabel;
    QLabel *dataBitsLabel;
    QLabel *parityLabel;
    QLabel *stopBitsLabel;
    QLabel *flowControlLabel;

    QComboBox *baudRateBox;
    QComboBox *dataBitsBox;
    QComboBox *parityBox;
    QComboBox *stopBitsBox;
    QComboBox *flowControlBox;

#if ENABLE_DEVELOPER_MODE
    QLabel *plotYAxisMinThresholdLabel;
    QLabel *plotYAxisMaxThresholdLabel;
    QDoubleSpinBox *plotYAxisMinThresholdDoubleSpinBox;
    QDoubleSpinBox *plotYAxisMaxThresholdDoubleSpinBox;

    QLabel *floatPrecisionLabel;
    QSpinBox *floatPrecisionSpinBox;
#endif // ENABLE_DEVELOPER_MODE

    Settings currentSettings;
    QIntValidator *intValidator = nullptr;

    // People detection
    PeopleDetection *mPeopledetection = nullptr;
};

#endif // MAINWINDOW_H
