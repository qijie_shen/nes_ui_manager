#include <QtDebug>

#include "mainwindow.h"

#include "config.h"
#include "peopledetection.h"

#if defined(Q_OS_WIN32)
#include <time.h>
#include <windows.h>
#elif defined(Q_OS_LINUX)
#include <wiringPi.h>
#include <wiringPiI2C.h>

#include <stdlib.h>
#define QT_ON_RPI
#endif

static const char blank_string[] = QT_TRANSLATE_NOOP("MainWindow", "N/A");

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    mSerial(new QSerialPort(this)),
    intValidator(new QIntValidator(0, 4000000, this)),
    mPeopledetection(new PeopleDetection(this, mSerial))
{
    uint8_t i;

    this->setupUi();
    //setGeometry(400, 250, 542, 390);

    mStatusLabel = new QLabel;
    statusBar->addWidget(mStatusLabel);

    baudRateBox->setInsertPolicy(QComboBox::NoInsert);

    connectButton->setEnabled(true);
    disconnectButton->setEnabled(false);

    connect(applyButton, &QPushButton::clicked, this, &MainWindow::apply);
    connect(connectButton, &QPushButton::clicked, this, &MainWindow::openSerialPort);
    connect(disconnectButton, &QPushButton::clicked, this, &MainWindow::closeSerialPort);

    connect(serialPortInfoListBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::showPortInfo);
    connect(baudRateBox,  QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::checkCustomBaudratePolicy);
    connect(serialPortInfoListBox, QOverload<int>::of(&QComboBox::currentIndexChanged), this, &MainWindow::checkCustomDevicePathPolicy);

    connect(mSerial, &QSerialPort::errorOccurred, this, &MainWindow::handleError);
    connect(mSerial, &QSerialPort::readyRead, this, &MainWindow::readData);

    fillPortsParameters();
    fillPortsInfo();
    updateSettings();

    ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED_INDEX] = true;
    for (i=1; i<GENERAL_ISSUE_NUMBER; i++)
    {
        ifSystemHasIssuesArray[i] = false;
    }

    // Check dirs and create dirs if needed
    if (!pnnlLoggingFilesDir.exists(PNNL_DIR_NAME_STRING))
    {
        pnnlLoggingFilesDir.mkdir(PNNL_DIR_NAME_STRING);
    }
    pnnlLoggingFilesDir = PNNL_DIR_NAME_STRING;

    if (!sleepirLoggingFilesDir.exists(SLEEPIR_DIR_NAME_STRING))
    {
        sleepirLoggingFilesDir.mkdir(SLEEPIR_DIR_NAME_STRING);
    }
    sleepirLoggingFilesDir = SLEEPIR_DIR_NAME_STRING;

    pnnlOccupancyResultsLoggingFile.setFileName(pnnlLoggingFilesDir.filePath(QString().sprintf("%s%s",
                                                                                               PNNL_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING,
                                                                                               DEFAULT_LOGGING_FILE_TYPE_STRING)));

    if(pnnlOccupancyResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
    {
        pnnlOccupancyResultsLoggingFile.close();
    }
}

MainWindow::~MainWindow()
{

}

// ui functions
void MainWindow::setupUi(void)
{
    if (this->objectName().isEmpty())
        this->setObjectName(QString::fromUtf8("MainWindow"));
#if ((defined CONFIG_MAINWINDOW_SCREEN_WIDTH) && (defined CONFIG_MAINWINDOW_SCREEN_HEIGHT))
    this->resize(CONFIG_MAINWINDOW_SCREEN_WIDTH, CONFIG_MAINWINDOW_SCREEN_HEIGHT);
#else
    this->resize(400, 300); // default setup
#endif // ((defined CONFIG_MAINWINDOW_SCREEN_WIDTH) && (defined CONFIG_MAINWINDOW_SCREEN_HEIGHT))
    menuBar = new QMenuBar(this);
    menuBar->setObjectName(QString::fromUtf8("menuBar"));
    this->setMenuBar(menuBar);
    mainToolBar = new QToolBar(this);
    mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
    this->addToolBar(mainToolBar);
    centralWidget = new QWidget(this);
    centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
    this->setCentralWidget(centralWidget);
    statusBar = new QStatusBar(this);
    statusBar->setObjectName(QString::fromUtf8("statusBar"));
    this->setStatusBar(statusBar);

    retranslateUi();

    QMetaObject::connectSlotsByName(this);

    centralGroupBox = new QGroupBox;
    setupInteractionUi();
    setupSettingsUi();

    centralHorizontalLayout = new QHBoxLayout;
    centralHorizontalLayout->addWidget(settingsGroupBox);
    centralHorizontalLayout->addWidget(interactionGroupBox);

    centralHorizontalLayout->setStretchFactor(settingsGroupBox, SETTINGS_GROUPBOX_LAYOUT_STRETCH_FACTOR);
    centralHorizontalLayout->setStretchFactor(interactionGroupBox, INTERACTION_GROUPBOX_LAYOUT_STRETCH_FACTOR);

    centralGroupBox->setLayout(centralHorizontalLayout);
    setCentralWidget(centralGroupBox);
}

void MainWindow::retranslateUi(void)
{
    this->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
}

void MainWindow::setupSettingsUi(void)
{
    settingsGroupBox = new QGroupBox(tr("Serial related"));
    settingsGroupBox->setObjectName(QString::fromUtf8("settingsGroupBox"));

    settingsVerticalLayout = new QVBoxLayout();
    settingsVerticalLayout->setObjectName(QString::fromUtf8("settingsVerticalLayout"));

    // Apply
    applyHorizontalLayout = new QHBoxLayout();
    applyHorizontalLayout->setObjectName(QString::fromUtf8("applyHorizontalLayout"));

    applyButton = new QPushButton(this);
    applyButton->setObjectName(QString::fromUtf8("applyButton"));

    connectButton = new QPushButton(this);
    connectButton->setObjectName(QString::fromUtf8("connectButton"));

    disconnectButton = new QPushButton(this);
    disconnectButton->setObjectName(QString::fromUtf8("disconnectButton"));

    applyHorizontalLayout->addWidget(connectButton);
    applyHorizontalLayout->addWidget(disconnectButton);

    // Serial selection
    serialSelectGroupBox = new QGroupBox;
    serialSelectGroupBox->setObjectName(QString::fromUtf8("serialSelectGroupBox"));

    serialSelectGridLayout = new QGridLayout(serialSelectGroupBox);
    serialSelectGridLayout->setObjectName(QString::fromUtf8("serialSelectGridLayout"));

    serialPortInfoListBox = new QComboBox(serialSelectGroupBox);
    serialPortInfoListBox->setObjectName(QString::fromUtf8("serialPortInfoListBox"));
    serialSelectGridLayout->addWidget(serialPortInfoListBox, 0, 0, 1, 1);

    descriptionLabel = new QLabel(serialSelectGroupBox);
    descriptionLabel->setObjectName(QString::fromUtf8("descriptionLabel"));
    serialSelectGridLayout->addWidget(descriptionLabel, 1, 0, 1, 1);

    manufacturerLabel = new QLabel(serialSelectGroupBox);
    manufacturerLabel->setObjectName(QString::fromUtf8("manufacturerLabel"));
    serialSelectGridLayout->addWidget(manufacturerLabel, 2, 0, 1, 1);

    serialNumberLabel = new QLabel(serialSelectGroupBox);
    serialNumberLabel->setObjectName(QString::fromUtf8("serialNumberLabel"));
    serialSelectGridLayout->addWidget(serialNumberLabel, 3, 0, 1, 1);

    locationLabel = new QLabel(serialSelectGroupBox);
    locationLabel->setObjectName(QString::fromUtf8("locationLabel"));
    serialSelectGridLayout->addWidget(locationLabel, 4, 0, 1, 1);

    vidLabel = new QLabel(serialSelectGroupBox);
    vidLabel->setObjectName(QString::fromUtf8("vidLabel"));
    serialSelectGridLayout->addWidget(vidLabel, 5, 0, 1, 1);

    pidLabel = new QLabel(serialSelectGroupBox);
    pidLabel->setObjectName(QString::fromUtf8("pidLabel"));
    serialSelectGridLayout->addWidget(pidLabel, 6, 0, 1, 1);

    // Serial parameters
    parametersBox = new QGroupBox;
    parametersBox->setObjectName(QString::fromUtf8("parametersBox"));

    parametersGridLayout = new QGridLayout(parametersBox);
    parametersGridLayout->setObjectName(QString::fromUtf8("parametersGridLayout"));
#if 0
    hostText = new QLineEdit(parametersBox);
    hostText->setObjectName(QString::fromUtf8("hostText"));
    parametersGridLayout->addWidget(hostText, 5, 1, 1, 1);

    portText = new QLineEdit(parametersBox);
    portText->setObjectName(QString::fromUtf8("port_text"));
    parametersGridLayout->addWidget(portText, 6, 1, 1, 1);

    hostLabel = new QLabel(parametersBox);
    hostLabel->setObjectName(QString::fromUtf8("hostLabel"));
    parametersGridLayout->addWidget(hostLabel, 10, 0, 1, 1);

    portLabel = new QLabel(parametersBox);
    portLabel->setObjectName(QString::fromUtf8("portLabel"));
    parametersGridLayout->addWidget(portLabel, 11, 0, 1, 1);
#endif

    dataBitsLabel = new QLabel(parametersBox);
    dataBitsLabel->setObjectName(QString::fromUtf8("dataBitsLabel"));
    parametersGridLayout->addWidget(dataBitsLabel, 1, 0, 1, 1);

    dataBitsBox = new QComboBox(parametersBox);
    dataBitsBox->setObjectName(QString::fromUtf8("dataBitsBox"));
    parametersGridLayout->addWidget(dataBitsBox, 1, 1, 1, 1);

    baudRateLabel = new QLabel(parametersBox);
    baudRateLabel->setObjectName(QString::fromUtf8("baudRateLabel"));
    parametersGridLayout->addWidget(baudRateLabel, 0, 0, 1, 1);

    baudRateBox = new QComboBox(parametersBox);
    baudRateBox->setObjectName(QString::fromUtf8("baudRateBox"));
    parametersGridLayout->addWidget(baudRateBox, 0, 1, 1, 1);

    parityLabel = new QLabel(parametersBox);
    parityLabel->setObjectName(QString::fromUtf8("parityLabel"));
    parametersGridLayout->addWidget(parityLabel, 2, 0, 1, 1);

    parityBox = new QComboBox(parametersBox);
    parityBox->setObjectName(QString::fromUtf8("parityBox"));
    parametersGridLayout->addWidget(parityBox, 2, 1, 1, 1);

    stopBitsLabel = new QLabel(parametersBox);
    stopBitsLabel->setObjectName(QString::fromUtf8("stopBitsLabel"));
    parametersGridLayout->addWidget(stopBitsLabel, 3, 0, 1, 1);

    stopBitsBox = new QComboBox(parametersBox);
    stopBitsBox->setObjectName(QString::fromUtf8("stopBitsBox"));
    parametersGridLayout->addWidget(stopBitsBox, 3, 1, 1, 1);

    flowControlLabel = new QLabel(parametersBox);
    flowControlLabel->setObjectName(QString::fromUtf8("flowControlLabel"));
    parametersGridLayout->addWidget(flowControlLabel, 4, 0, 1, 1);

    flowControlBox = new QComboBox(parametersBox);
    flowControlBox->setObjectName(QString::fromUtf8("flowControlBox"));
    parametersGridLayout->addWidget(flowControlBox, 4, 1, 1, 1);

#if ENABLE_DEVELOPER_MODE
    // Other parameters
    plotYAxisMaxThresholdLabel = new QLabel(parametersBox);
    plotYAxisMaxThresholdLabel->setObjectName(QString::fromUtf8("plotYAxisMaxThresholdLabel"));
    parametersGridLayout->addWidget(plotYAxisMaxThresholdLabel, 10, 0, 1, 1);

    plotYAxisMaxThresholdDoubleSpinBox = new QDoubleSpinBox(parametersBox);
    plotYAxisMaxThresholdDoubleSpinBox->setObjectName(QString::fromUtf8("plotYAxisMaxThresholdDoubleSpinBox"));
    parametersGridLayout->addWidget(plotYAxisMaxThresholdDoubleSpinBox, 10, 1, 1, 1);

    plotYAxisMinThresholdLabel = new QLabel(parametersBox);
    plotYAxisMinThresholdLabel->setObjectName(QString::fromUtf8("plotYAxisMinThresholdLabel"));
    parametersGridLayout->addWidget(plotYAxisMinThresholdLabel, 11, 0, 1, 1);

    plotYAxisMinThresholdDoubleSpinBox = new QDoubleSpinBox(parametersBox);
    plotYAxisMinThresholdDoubleSpinBox->setObjectName(QString::fromUtf8("plotYAxisMinThresholdDoubleSpinBox"));
    parametersGridLayout->addWidget(plotYAxisMinThresholdDoubleSpinBox, 11, 1, 1, 1);

    floatPrecisionLabel = new QLabel(parametersBox);
    floatPrecisionLabel->setObjectName(QString::fromUtf8("floatPrecisionLabel"));
    parametersGridLayout->addWidget(floatPrecisionLabel, 12, 0, 1, 1);

    floatPrecisionSpinBox = new QSpinBox(parametersBox);
    floatPrecisionSpinBox->setObjectName(QString::fromUtf8("floatPrecisionSpinBox"));
    floatPrecisionSpinBox->setMinimum(2);
    floatPrecisionSpinBox->setMaximum(6);
    floatPrecisionSpinBox->setValue(4);
    parametersGridLayout->addWidget(floatPrecisionSpinBox, 12, 1, 1, 1);
#endif // ENABLE_DEVELOPER_MODE

    settingsVerticalLayout->addWidget(serialSelectGroupBox);
    settingsVerticalLayout->addWidget(parametersBox);
    settingsVerticalLayout->addWidget(applyButton);
    settingsVerticalLayout->addLayout(applyHorizontalLayout);
    settingsGroupBox->setLayout(settingsVerticalLayout);

    retranslateSettingsUi();

    QMetaObject::connectSlotsByName(this);
}

void MainWindow::retranslateSettingsUi(void)
{
    applyButton->setText(QApplication::translate("Mainwindows", "Apply settings", nullptr));
    connectButton->setText(QApplication::translate("Mainwindows", "Connect", nullptr));
    disconnectButton->setText(QApplication::translate("Mainwindows", "Disconnect", nullptr));

    serialSelectGroupBox->setTitle(QApplication::translate("Mainwindows", "Select Serial Port", nullptr));
    descriptionLabel->setText(QApplication::translate("Mainwindows", "Description:", nullptr));
    manufacturerLabel->setText(QApplication::translate("Mainwindows", "Manufacturer:", nullptr));
    serialNumberLabel->setText(QApplication::translate("Mainwindows", "Serial number:", nullptr));
    locationLabel->setText(QApplication::translate("Mainwindows", "Location:", nullptr));
    vidLabel->setText(QApplication::translate("Mainwindows", "Vendor ID:", nullptr));
    pidLabel->setText(QApplication::translate("Mainwindows", "Product ID:", nullptr));

    parametersBox->setTitle(QApplication::translate("Mainwindows", "Select Parameters", nullptr));
    baudRateLabel->setText(QApplication::translate("Mainwindows", "BaudRate:", nullptr));
    dataBitsLabel->setText(QApplication::translate("Mainwindows", "Data bits:", nullptr));
    parityLabel->setText(QApplication::translate("Mainwindows", "Parity:", nullptr));
    stopBitsLabel->setText(QApplication::translate("Mainwindows", "Stop bits:", nullptr));
    flowControlLabel->setText(QApplication::translate("Mainwindows", "Flow control:", nullptr));

#if 0
    hostLabel->setText(QApplication::translate("Mainwindows", "Host_IP", nullptr));
    portLabel->setText(QApplication::translate("Mainwindows", "Host_Port", nullptr));
#endif

#if ENABLE_DEVELOPER_MODE
    plotYAxisMaxThresholdLabel->setText(QApplication::translate("Mainwindows", "Plot_Y_Max", nullptr));
    plotYAxisMinThresholdLabel->setText(QApplication::translate("Mainwindows", "Plot_Y_Min", nullptr));

    floatPrecisionLabel->setText(QApplication::translate("Mainwindows", "FloatPrecision", nullptr));
#endif // ENABLE_DEVELOPER_MODE
}

void MainWindow::setupInteractionUi(void)
{
    QVBoxLayout *mainLayout = new QVBoxLayout;
    // H layout for the first line
    QHBoxLayout *horizontalLayout = new QHBoxLayout;
    interactionGroupBox = new QGroupBox(tr(INTERACTION_GROUP_BOX_NAME));

    createDataTimeGroupBox();
    // createMaterialSelectionGroupBox
    createMiscStatusIndicationGroupBox();
    // createTestingGroupBox
    createNetworkStatusGroupBox();

    // First line
    horizontalLayout->addWidget(dateTimeGroupBox);
    //horizontalLayout->addWidget(areasMaterialTypeGroupBox);
    horizontalLayout->addWidget(miscStatusIndicationGroupBox);

    mainLayout->addLayout(horizontalLayout);
    //mainLayout->addWidget(testingGroupBox);
    mainLayout->addWidget(nodesConnectionGroupBox);
    interactionGroupBox->setLayout(mainLayout);

    // Init the RTC
#if defined(Q_OS_LINUX)
    #if 0 //def QT_ON_RPI
    if(wiringPiSetup() < 0)
    {
        printf("wiringPiSetup failed\n");
        cout << flush;
    }
    else
    {
        pinMode(1, OUTPUT);
    }
    #endif // QT_ON_RPI
    #if 0
    timeCalibrationFd = -1;

    if(wiringPiSetup() < 0)
    {
        printf("wiringPiSetup failed\n");
        cout << flush;
    }
    else
    {
        timeCalibrationFd = wiringPiI2CSetup(DS3231_ADDRESS);
        printf("timeCalibrationFd is %d\n", timeCalibrationFd);
    }
    #endif
#endif // Q_OS_LINUX
}

void MainWindow::retranslateInteractionUi(void)
{

}

void MainWindow::createDataTimeGroupBox(void)
{
    dateTimeGroupBox = new QGroupBox(tr(DATE_TIME_GROUP_BOX_NAME));
    QHBoxLayout *horizontalLayout1 = new QHBoxLayout;
    QHBoxLayout *horizontalLayout2 = new QHBoxLayout;
    QHBoxLayout *horizontalLayout3 = new QHBoxLayout;
    QVBoxLayout *verticalLayout = new QVBoxLayout;

    // Refresh timer setup
    QTimer *timeRefreshTimer = new QTimer(this);
    timeRefreshTimer->start(CLOCK_DISPLAY_REFRESH_PERIOD);
    connect(timeRefreshTimer, SIGNAL(timeout()), this, SLOT(systemTimeDisplayUpdate()));

    // First line
    QLabel *clockDisplayIndicationLabel = new QLabel(CLOCK_DISPLAY_LABEL_STRING);
    currentDateTimeLabel = new QLabel;
    horizontalLayout1->addWidget(clockDisplayIndicationLabel);
    horizontalLayout1->addWidget(currentDateTimeLabel);

    // Second line
    QLabel *setDateTimeIndicationLabel = new QLabel(tr(SET_CLOCK_LABEL_STRING));
    dateTimeEdit = new QDateTimeEdit;
    dateTimeEdit->setDisplayFormat(QApplication::translate("TestTime", "yyyy-MM-dd HH:mm:ss", nullptr));
    horizontalLayout2->addWidget(setDateTimeIndicationLabel);
    horizontalLayout2->addWidget(dateTimeEdit);

    // Third line
    getCurrentTimeForEditPushButton = new QPushButton(tr(GET_CURRENT_TIME_BUTTON_NAME));
    setNewSystemTimePushButton = new QPushButton(tr(SET_NEW_TIME_BUTTON_NAME));

    // Button connections
    connect(getCurrentTimeForEditPushButton, SIGNAL(clicked()), this, SLOT(getCurrentTimeToEdit()));
    connect(setNewSystemTimePushButton, SIGNAL(clicked()), this, SLOT(setSystemTimeFromDateTimeEdit()));
    horizontalLayout3->addWidget(getCurrentTimeForEditPushButton);
    horizontalLayout3->addWidget(setNewSystemTimePushButton);

    // Total
    verticalLayout->addLayout(horizontalLayout1);
    verticalLayout->addLayout(horizontalLayout2);
    verticalLayout->addLayout(horizontalLayout3);

    dateTimeGroupBox->setLayout(verticalLayout);
}

void MainWindow::createMiscStatusIndicationGroupBox(void)
{
    miscStatusIndicationGroupBox = new QGroupBox(tr(MISC_STATUS_INDICATION_GROUP_BOX_NAME));
    QHBoxLayout *horizontalLayout1 = new QHBoxLayout;
    QHBoxLayout *horizontalLayout2 = new QHBoxLayout;
    QHBoxLayout *horizontalLayout3 = new QHBoxLayout;
    QVBoxLayout *verticalLayout = new QVBoxLayout;

    // First line
    QLabel *serialConnectionStatusDecLabel = new QLabel(SERIAL_CONNECTION_STATUS_DEC_LABEL_CONTENT);
    serialConnectionStatusLabel = new QLabel(tr(SERIAL_CONNECTION_STATUS_NULL_CONTENT));
    horizontalLayout1->addWidget(serialConnectionStatusDecLabel);
    horizontalLayout1->addWidget(serialConnectionStatusLabel);

    // Second line
    QLabel *systemIssueDetectionResultDecLabel = new QLabel(tr(GENERAL_ISSUE_DETECTION_RESULT_DEC_LABEL_CONTENT));
    systemIssueDetectionResultLabel = new QLabel(tr(GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED));
    horizontalLayout2->addWidget(systemIssueDetectionResultDecLabel);
    horizontalLayout2->addWidget(systemIssueDetectionResultLabel);

    // Third line
    QLabel *occupancyDetectionStatusDecLabel = new QLabel(tr(OCCUPANCY_DETECTION_RESULT_DEC_LABEL_CONTENT));
    occupancyDetectionResultLabel = new QLabel(tr(OCCUPANCY_DETECTION_RESULT_NULL_CONTENT));
    horizontalLayout3->addWidget(occupancyDetectionStatusDecLabel);
    horizontalLayout3->addWidget(occupancyDetectionResultLabel);

    // Total
    verticalLayout->addLayout(horizontalLayout1);
    verticalLayout->addLayout(horizontalLayout2);
    verticalLayout->addLayout(horizontalLayout3);

    miscStatusIndicationGroupBox->setLayout(verticalLayout);
}

void MainWindow::createNetworkStatusGroupBox(void)
{
    int i;

    nodesConnectionGroupBox = new QGroupBox(tr(CONNECTION_STATUS_GROUP_BOX_NAME));
    QVBoxLayout *verticalLayout = new QVBoxLayout;
    QHBoxLayout *horizontalLayout;
    QLabel *areaIDLabel;
    QLabel *nodeAddressDecLabel;
    QLabel *connectionStatusDecLabel;
    QLabel *nodeResetStatusDecLabel;
    QLabel *nodeDetectionResultDecLabel;

    for (i=0; i<MAX_NODE_NUMBER; i++)
    {
        horizontalLayout = new QHBoxLayout;
        areaIDLabel = new QLabel(QString().sprintf("%s%d",
                                                   CONNECTION_STATUS_AREA_ID_DCE_LABEL_CONTENT,
                                                   (i+1)));
        nodeAddressDecLabel = new QLabel(tr(CONNECTION_STATUS_NODE_ADDRESS_DEC_LABEL_CONTENT));

        InteractionNodes[i].address = DEFAULT_NODE_ADDRESS;
        InteractionNodes[i].addressLabel = new QLabel(QString().sprintf("0x%4.4x", DEFAULT_NODE_ADDRESS));;
        InteractionNodes[i].connectionStatusLabel = new QLabel(tr(CONNECTION_STATUS_DISCONNECTED_LABEL_CONTENT));
        InteractionNodes[i].resetPushButton = new QPushButton(tr(CONNECTION_STATUS_RESET_BUTTON_LABEL_CONTENT));
        InteractionNodes[i].resetStatusLabel = new QLabel(tr(CONNECTION_STATUS_NULL_RESET_STATUS_LABEL_CONTENT));
        InteractionNodes[i].detectionResultLabel = new QLabel(tr(NODE_DETECTION_RESULT_NULL));;

        connectionStatusDecLabel = new QLabel(tr(CONNECTION_STATUS_DEC_LABEL_CONTENT));
        nodeResetStatusDecLabel = new QLabel(tr(CONNECTION_STATUS_INIT_STATUS_DEC_LABEL_CONTENT));
        nodeDetectionResultDecLabel = new QLabel(tr(CONNECTION_STATUS_DETECTION_RESULT_DEC_LABEL_CONTENT));

        horizontalLayout->addWidget(areaIDLabel);
        horizontalLayout->addWidget(nodeAddressDecLabel);
        horizontalLayout->addWidget(InteractionNodes[i].addressLabel);
        horizontalLayout->addWidget(connectionStatusDecLabel);
        horizontalLayout->addWidget(InteractionNodes[i].connectionStatusLabel);
        horizontalLayout->addWidget(InteractionNodes[i].resetPushButton);
        horizontalLayout->addWidget(nodeResetStatusDecLabel);
        horizontalLayout->addWidget(InteractionNodes[i].resetStatusLabel);
        horizontalLayout->addWidget(nodeDetectionResultDecLabel);
        horizontalLayout->addWidget(InteractionNodes[i].detectionResultLabel);

        verticalLayout->addLayout(horizontalLayout);
    }

    nodesConnectionGroupBox->setLayout(verticalLayout);
}

// Private slots
void MainWindow::systemTimeDisplayUpdate(void)
{
#if 0
    static bool ifHigh = false;
#endif
    currentDateTimeLabel->setText(QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss"));
    if (ifDateTimeCalibrated == false)
    {
        if (ifDateTimeCalibratedMboxPopupCounter == DATE_TIME_UPDATE_WAIT_SECS_TO_POPUP_MBOX_COUNTER)
        {
            QMessageBox msgBox;
            msgBox.setText("Please calibrate the system time manually!");
            msgBox.exec();
            ifDateTimeCalibratedMboxPopupCounter = 0;
        }
        else
        {
            ifDateTimeCalibratedMboxPopupCounter++;
        }
    }
    else
    {
        QString tempCurrentDateString = QDate::currentDate().toString("yyyy-MM-dd");
        if (QString::compare(tempCurrentDateString, currentDateString) != 0)
        {
            currentDateString = tempCurrentDateString;
            // update all the filenames
            emit updateDatePartOfFileNames(currentDateString);
            // change local logging fileName
            QString fileName;
            fileName.sprintf("%s%s%s",
                            currentDateString.toLatin1().data(),
                            SLEEPIR_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING,
                            DEFAULT_LOGGING_FILE_TYPE_STRING);
            occupancyResultsLoggingFile.setFileName(fileName);
            if (occupancyResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
            {
                occupancyResultsLoggingFile.close();
            }
        }
    }
#if 0
    if (ifHigh)
    {
        #if defined(Q_OS_LINUX)
        #ifdef QT_ON_RPI
        digitalWrite(1, HIGH);
        #endif // QT_ON_RPI
        #endif

        ifHigh = false;
    }
    else
    {
        #if defined(Q_OS_LINUX)
        #ifdef QT_ON_RPI
        digitalWrite(1, LOW);
        #endif // QT_ON_RPI
        #endif

        ifHigh = true;
    }
#endif
}

void MainWindow::getCurrentTimeToEdit(void)
{
    dateTimeEdit->setDateTime(QDateTime::currentDateTime());
}

void MainWindow::setSystemTimeFromDateTimeEdit(void)
{
    QDateTime dateTimeToSet = dateTimeEdit->dateTime();
#if defined(Q_OS_WIN32)
    SYSTEMTIME st;

    st.wYear = static_cast<WORD>(dateTimeToSet.date().year());
    st.wMonth = static_cast<WORD>(dateTimeToSet.date().month());
    st.wDay = static_cast<WORD>(dateTimeToSet.date().day());
    st.wHour = static_cast<WORD>(dateTimeToSet.time().hour());
    st.wMinute = static_cast<WORD>(dateTimeToSet.time().minute());
    st.wSecond = static_cast<WORD>(dateTimeToSet.time().second());
    st.wMilliseconds = static_cast<WORD>(dateTimeToSet.time().msec());

    // Needs the administrator's permission to run successfully
    // UTC
    //SetSystemTime(&st);
    // Local
    SetLocalTime(&st);
#elif defined(Q_OS_LINUX)

    #if 0

    struct timeval tv;
    struct tm currentTm;

    currentTm.tm_year = dateTimeToSet.date().year();
    currentTm.tm_mon  = dateTimeToSet.date().month();
    currentTm.tm_mday = dateTimeToSet.date().day();
    currentTm.tm_hour = dateTimeToSet.time().hour();
    currentTm.tm_min = dateTimeToSet.time().minute();
    currentTm.tm_sec  = dateTimeToSet.time().second();

    tv.tv_sec = mktime(&currentTm);
    tv.tv_usec = dateTimeToSet.time().msec() * 1000;

    if (settimeofday(&tv, NULL) == -1)
    {
        perror("settimeofday");
    }

    printf("BCD code of time: 0x%2.2d, 0x%2.2d, 0x%2.2d, 0x%2.2d, 0x%2.2d, 0x%2.2d\n",
            (dateTimeToSet.date().year() - 2000),
            dateTimeToSet.date().month(),
            dateTimeToSet.date().day(),
            dateTimeToSet.time().hour(),
            dateTimeToSet.time().minute(),
            dateTimeToSet.time().second());
    cout << flush;
    #endif

    QString dateTimeSetupString;
    dateTimeSetupString.sprintf("sudo date -s \"%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d\"",
                                dateTimeToSet.date().year(),
                                dateTimeToSet.date().month(),
                                dateTimeToSet.date().day(),
                                dateTimeToSet.time().hour(),
                                dateTimeToSet.time().minute(),
                                dateTimeToSet.time().second());
    system(dateTimeSetupString.toLatin1().data());
#endif // Q_OS_WIN32
    // update the datetime edit
    currentDateTimeLabel->setText(dateTimeToSet.toString("yyyy-MM-dd hh:mm:ss"));
    if (ifDateTimeCalibrated == false)
    {
        currentDateString = dateTimeToSet.date().toString("yyyy-MM-dd");

        occupancyResultsLoggingFile.setFileName(sleepirLoggingFilesDir.filePath(QString().sprintf("%s%s%s",
                                                                                                  currentDateString.toLatin1().data(),
                                                                                                  SLEEPIR_OCCUPANCY_RESULTS_LOGGING_FILE_NAME_STRING,
                                                                                                  DEFAULT_LOGGING_FILE_TYPE_STRING)));
        if (occupancyResultsLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
        {
            occupancyResultsLoggingFile.close();
        }

        hubRawDataLoggingFile.setFileName(sleepirLoggingFilesDir.filePath(QString().sprintf("%s%s%s",
                                                                                                  currentDateString.toLatin1().data(),
                                                                                                  SLEEPIR_HUB_RAW_DATA_LOGGING_FILE_NAME_STRING,
                                                                                                  DEFAULT_LOGGING_FILE_TYPE_STRING)));
        if (hubRawDataLoggingFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
        {
            hubRawDataLoggingFile.close();
        }
    }
    ifDateTimeCalibrated = true;

    QMessageBox msgBox;
    msgBox.setText(TIME_CALIBRATION_IS_DONE_CONTENT);
    msgBox.exec();
}

void MainWindow::showPortInfo(int idx)
{
    if (idx == -1)
    {
        return;
    }

    const QStringList list = serialPortInfoListBox->itemData(idx).toStringList();
    descriptionLabel->setText(tr("Description: %1").arg(list.count() > 1 ? list.at(1) : tr(blank_string)));
    manufacturerLabel->setText(tr("Manufacturer: %1").arg(list.count() > 2 ? list.at(2) : tr(blank_string)));
    serialNumberLabel->setText(tr("Serial number: %1").arg(list.count() > 3 ? list.at(3) : tr(blank_string)));
    locationLabel->setText(tr("Location: %1").arg(list.count() > 4 ? list.at(4) : tr(blank_string)));
    vidLabel->setText(tr("Vendor Identifier: %1").arg(list.count() > 5 ? list.at(5) : tr(blank_string)));
    pidLabel->setText(tr("Product Identifier: %1").arg(list.count() > 6 ? list.at(6) : tr(blank_string)));
}

void MainWindow::apply()
{
    updateSettings();
}

void MainWindow::checkCustomBaudratePolicy(int idx)
{
    const bool isCustomBaudRate = !baudRateBox->itemData(idx).isValid();
    baudRateBox->setEditable(isCustomBaudRate);
    if (isCustomBaudRate)
    {
        baudRateBox->clearEditText();
        QLineEdit *edit = baudRateBox->lineEdit();
        edit->setValidator(intValidator);
    }
}

void MainWindow::checkCustomDevicePathPolicy(int idx)
{
    const bool isCustomPath = !serialPortInfoListBox->itemData(idx).isValid();
    serialPortInfoListBox->setEditable(isCustomPath);
    if (isCustomPath)
    {
        serialPortInfoListBox->clearEditText();
    }
}

void MainWindow::fillPortsParameters()
{
    baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    baudRateBox->addItem(QStringLiteral("9600"), QSerialPort::Baud9600);
    baudRateBox->addItem(QStringLiteral("19200"), QSerialPort::Baud19200);
    baudRateBox->addItem(QStringLiteral("38400"), QSerialPort::Baud38400);
    baudRateBox->addItem(tr("Custom"));

    dataBitsBox->addItem(QStringLiteral("5"), QSerialPort::Data5);
    dataBitsBox->addItem(QStringLiteral("6"), QSerialPort::Data6);
    dataBitsBox->addItem(QStringLiteral("7"), QSerialPort::Data7);
    dataBitsBox->addItem(QStringLiteral("8"), QSerialPort::Data8);
    dataBitsBox->setCurrentIndex(3);

    parityBox->addItem(tr("None"), QSerialPort::NoParity);
    parityBox->addItem(tr("Even"), QSerialPort::EvenParity);
    parityBox->addItem(tr("Odd"), QSerialPort::OddParity);
    parityBox->addItem(tr("Mark"), QSerialPort::MarkParity);
    parityBox->addItem(tr("Space"), QSerialPort::SpaceParity);

    stopBitsBox->addItem(QStringLiteral("1"), QSerialPort::OneStop);
#ifdef Q_OS_WIN
    stopBitsBox->addItem(tr("1.5"), QSerialPort::OneAndHalfStop);
#endif
    stopBitsBox->addItem(QStringLiteral("2"), QSerialPort::TwoStop);

    flowControlBox->addItem(tr("None"), QSerialPort::NoFlowControl);
    flowControlBox->addItem(tr("RTS/CTS"), QSerialPort::HardwareControl);
    flowControlBox->addItem(tr("XON/XOFF"), QSerialPort::SoftwareControl);
}

void MainWindow::fillPortsInfo()
{
    serialPortInfoListBox->clear();
    QString description;
    QString manufacturer;
    QString serialNumber;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos)
    {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blank_string)
             << (!manufacturer.isEmpty() ? manufacturer : blank_string)
             << (!serialNumber.isEmpty() ? serialNumber : blank_string)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blank_string)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blank_string);

        serialPortInfoListBox->addItem(list.first(), list);
    }

    serialPortInfoListBox->addItem(tr("Custom"));
}

void MainWindow::updateSettings()
{
    currentSettings.name = serialPortInfoListBox->currentText();

    if (baudRateBox->currentIndex() == 4)
    {
        currentSettings.baudRate = baudRateBox->currentText().toInt();
    }
    else
    {
        currentSettings.baudRate = static_cast<QSerialPort::BaudRate>(baudRateBox->itemData(baudRateBox->currentIndex()).toInt());
    }
    currentSettings.stringBaudRate = QString::number(currentSettings.baudRate);

    currentSettings.dataBits = static_cast<QSerialPort::DataBits>(dataBitsBox->itemData(dataBitsBox->currentIndex()).toInt());
    currentSettings.stringDataBits = dataBitsBox->currentText();

    currentSettings.parity = static_cast<QSerialPort::Parity>(parityBox->itemData(parityBox->currentIndex()).toInt());
    currentSettings.stringParity = parityBox->currentText();

    currentSettings.stopBits = static_cast<QSerialPort::StopBits>(stopBitsBox->itemData(stopBitsBox->currentIndex()).toInt());
    currentSettings.stringStopBits = stopBitsBox->currentText();

    currentSettings.flowControl = static_cast<QSerialPort::FlowControl>(flowControlBox->itemData(flowControlBox->currentIndex()).toInt());
    currentSettings.stringFlowControl = flowControlBox->currentText();

#ifdef TCP_SERVER_SUPPORT
    if (host_text->text().length() == 0)
    {
        currentSettings.hostIP = QString("127.0.0.1");
    }
    else
    {
        currentSettings.hostIP = host_text->text();
    }

    if (port_text->text().length() == 0)
    {
        currentSettings.port = QString(DEFAULT_TCP_SERVER_HOST_PORT);
    }
    else
    {
        currentSettings.port = port_text->text();
    }
#endif // TCP_SERVER_SUPPORT

#if ENABLE_DEVELOPER_MODE
    currentSettings.plotYAxisMinThresholdValue = plotYAxisMinThresholdDoubleSpinBox->text().toDouble();
    currentSettings.plotYAxisMaxThresholdValue = plotYAxisMaxThresholdDoubleSpinBox->text().toDouble();

    currentSettings.floatPrecision = floatPrecisionSpinBox->text().toInt();
#endif // ENABLE_DEVELOPER_MODE
}

void MainWindow::openSerialPort()
{
    mSerial->setPortName(currentSettings.name);
    mSerial->setBaudRate(currentSettings.baudRate);
    mSerial->setDataBits(currentSettings.dataBits);
    mSerial->setParity(currentSettings.parity);
    mSerial->setStopBits(currentSettings.stopBits);
    mSerial->setFlowControl(currentSettings.flowControl);
    if (mSerial->open(QIODevice::ReadWrite))
    {
        showStatusMessage(tr("Connected to %1 : %2, %3, %4, %5, %6")
                          .arg(currentSettings.name).arg(currentSettings.stringBaudRate).arg(currentSettings.stringDataBits)
                          .arg(currentSettings.stringParity).arg(currentSettings.stringStopBits).arg(currentSettings.stringFlowControl));
        connectButton->setEnabled(false);
        disconnectButton->setEnabled(true);
        ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED_INDEX] = false;
        occupancyDetectionResultLabel->setText(GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED);

        uint8_t i=0;
        for (; i<GENERAL_ISSUE_NUMBER; i++)
        {
            if (ifSystemHasIssuesArray[i] == true)
            {
                systemIssueDetectionResultLabel->setText(systemHasIssuesStringsArray[i]);
                break;
            }
        }
        if (i == GENERAL_ISSUE_NUMBER)
        {
            systemIssueDetectionResultLabel->setText(GENERAL_ISSUE_DETECTION_RESULT_NONE);
        }
    }
    else
    {
        QMessageBox::critical(this, tr("Error"), mSerial->errorString());

        showStatusMessage(tr("Open error"));
        return;
    }

#ifdef TCP_SERVER_SUPPORT
#if UTILIZE_TCP_SERVER
    mPeopledetection->getTCPServer().start(QHostAddress(setting.hostIP), setting.port.toUShort());
#endif // UTILIZE_TCP_SERVER
#endif // TCP_SERVER_SUPPORT

#if ENABLE_DEVELOPER_MODE
    mPeopledetection->setPlotYAxisThresholds(setting.plotYAxisMinThresholdValue,
                                              setting.plotYAxisMaxThresholdValue);
    //mPeopledetection->setFloatPrecision(setting.floatPrecision);
#endif // ENABLE_DEVELOPER_MODE
}

void MainWindow::closeSerialPort()
{
    if (mSerial->isOpen())
    {
        connectButton->setEnabled(true);
        disconnectButton->setEnabled(false);

        mSerial->close();
        showStatusMessage(tr("Disconnected"));
        ifSystemHasIssuesArray[GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED_INDEX] = true;
        occupancyDetectionResultLabel->setText(GENERAL_ISSUE_DETECTION_RESULT_NOT_CONNECTED);
    }

#ifdef TCP_SERVER_SUPPORT
#if UTILIZE_TCP_SERVER
    mPeopledetection->getTCPServer().stop();
#endif
#endif // TCP_SERVER_SUPPORT
}

void MainWindow::readData()
{
    mPeopledetection->handleSerialData();
}

void MainWindow::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError)
    {
        QMessageBox::critical(this, tr("Critical Error"), mSerial->errorString());
        closeSerialPort();
    }
}

void MainWindow::showStatusMessage(const QString &message)
{
    mStatusLabel->setText(message);
}
